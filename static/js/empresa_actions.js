


// JQUERY AND COMMOND ACTIONS ----------------------------------------------------------------------

function sendit(e){
	e.preventDefault();
	forma = $('#empresaspt1').get(0);
	var data = new FormData(forma);
	$.ajax({url:'/addempresa',
			type:'POST',
			data:data,
			cache: false,
	        processData: false,
	        contentType:false,
    	    dataType: 'json',
			success:function(response){
				$('.errr').removeClass('errr');
				$('.alert').remove();
				if(response.errs){
					$.each(response.errs,function(err,i){
						$('#id_'+err).addClass('errr').after('<div class="alert"><i class="fa fa-bell-slash-o"></i>('+err+') '+i+'</div>');
					});

				}
				if(response.saved=='ok'){
					$('#id_pk').val(response.pk);
					empresa = response;
					$('#contact_block').removeClass('blind');
				}
			}

	});

}

function Atachphoto(e){
	e.preventDefault();
	$('#id_logo').click();
}


function handleFileSelect(evt) {
    var files = evt.target.files;
    tmptype = files[0].type;
    if(tmptype.indexOf('image')>-1){
    	var reader = new FileReader();
    	reader.onload = (function(thefile){
    		return function(e){
    			$('#photo').html('<img class="roundlogo" src="'+e.target.result+'" />');
    		}

    	})(files[0]);
    	reader.readAsDataURL(files[0]);
    }
  }



function attaching(e){
	e.preventDefault();
	var target_name = $(this).attr('href').replace('#','');
	$('#'+target_name).click();
	$('#'+target_name).change(function(e){
    var files = e.target.files;
    tmptype = files[0].type;
    if(tmptype.indexOf('image')>-1){
    	var reader = new FileReader();
    	reader.onload = (function(thefile){
    		return function(e){
    			$('#box_'+target_name).html('<img class="roundlogo" src="'+e.target.result+'" />');
    		}
    	})(files[0]);
    	reader.readAsDataURL(files[0]);
    }

	});
}


jQuery(document).ready(function(){
	empresa.pk = $('#id_pk').val();
	$('#atahcphoto').click(Atachphoto);
	document.getElementById('id_logo').addEventListener('change', handleFileSelect, true);
	$('#empresaspt1').submit(sendit);
	$('#adder').click(myapp.controller('CatalogoController').addItem);

	$('.photoattacher').click(attaching);

	$('#addcontacto_btn').click(function(e){
		e.preventDefault();
		$('#velo,#contact_form').show(200);

	});


	$('.cancel_close').click(function(e){
		e.preventDefault();
		$('#velo,#contact_form').hide(200);

	});

	$('.send_form').click(function(e){
		e.preventDefault();
		$(this).parents('form:first').submit();
	});


});


