# -*- coding: utf-8 -*-


from django.http import HttpResponse
from django.shortcuts import redirect, render, render_to_response
from django.template.context import RequestContext
from django.views.generic import View
from pecuarios.forms import * 
from manager.models import * 
from django.contrib.sites.models import Site
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
import os
from django.template.defaultfilters import slugify
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.forms.models import model_to_dict
from django.contrib.auth.models import User
import re
from django.db.models import Q
import urllib2




class VideosView(View):
    """View to retrieve companies list."""

    template = 'manager/magazine/videos/listado.html'

    def get(self, request):
        """Retrieve the list of the all companies."""
        page = request.GET.get('page', 1)
        portal = request.GET.get('portal', None)

        context = {}
        caps = Videos.objects.all()
        context['caps'] = caps

        response = render(request, self.template, context)
        return response

VideosView = VideosView.as_view()




class getVideosView(View):

    def post(self,request):
        victim = request.POST.get('pk',None)
        a = Videos.objects.get(pk=victim)        
        a.delete()
        return HttpResponse(a)

    def get(self, request):
        #page = request.GET.get('page', 1)
        #portal = request.GET.get('portal', None)
        queries = Q()
        options = {}
        for k in ('nombre_autor__icontains', 'email'):
            value = request.GET.get(k,None)

            if value:
                options[k]=value
                
                queries |= Q(**options)

        context = {}
        autores = Videos.objects.filter(*[queries],**{})
        arts = [{
                'pk':x.pk,
                'title':x.nombre_autor,
                'descp':x.desc_usuario
                }
                for x in autores ]

        response = simplejson.dumps(arts)
        response = HttpResponse(response)
        
        return response

getVideosView = getVideosView.as_view()



class FormVideosView(View):
    template = 'manager/magazine/videos/formulario.html'
    def get(self, request):
        #sites = Site.objects.all()

        ida = request.GET.get('id',None)
        if ida:
            instanced = Videos.objects.get(pk=ida)
        else:
            instanced = None


        #page = request.GET.get('page', 1)
        #portal = request.GET.get('portal', None)
        widgets = {
            'slug_nombre_autor':forms.HiddenInput(),
            'tipo':forms.HiddenInput(),
            'fotoprincipal':forms.HiddenInput(),
            'redes':forms.HiddenInput(),
        }

        f =  FormCreator()

        forma = f.advanced_form_to_model(modelo=Videos,
                                         excludes={'vistas','votacion','numvotos','info','privado','empresarel'},
                                         widgets=widgets)
        

        for x,y in forma.base_fields.iteritems():
            #y.widget_attrs={'class':'monolito'}
            (y.widget.attrs.update({'class':'form-control'}))
        forma = forma(instance=instanced)        
        context = {}
        context['forma']=forma
        context['id']=ida

        response = render(request, self.template, context)
        return response

FormVideosView = FormVideosView.as_view()



class addVideosView(View):
    #template = 'manager/magazine/form_autor.html'

    def post(self, request):
        #sites = Site.objects.all()
        data = request.POST.copy()
        #data['slug_nombre_autor']=slugify(data['nombre_autor'])
        idvideo = request.POST.get('video',None)
        if  idvideo:
            url = "http://vimeo.com/api/v2/video/%s.json"%(idvideo)
            response = urllib2.urlopen(url)
            datax = response.read()
            #data = data.replace('','')
            values = simplejson.loads(datax)
            data['vimeominiatura'] = values[0]['thumbnail_small']

        ida = request.POST.get('id',None)
        if ida:
            instanced = Videos.objects.get(pk=ida)
            mesagge = u'El registro se actualizó con éxito.'

        else:
            instanced = None
            mesagge = u'El registro se agregó con éxito.' 


        f =  FormCreator()

        forma = f.advanced_form_to_model(modelo=Videos,excludes={})
        forma = forma(data,instance=instanced)        
        context = {}

        if forma.is_valid():
            save_reg = forma.save()
            responde = {
                'saved':'ok',
                'datos':{'pk':save_reg.pk,
                         'mesagge':mesagge,
                        },
                'callback':'autorSaved'
            }
            
            return HttpResponse(simplejson.dumps(responde))
        else:
            returner = {'errors':forma.errors}
            return HttpResponse(simplejson.dumps(returner))
        
        response = HttpResponse({'algo sa':'lio mal'})


addVideosView = addVideosView.as_view()