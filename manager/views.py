# -*- coding: utf-8 -*-


from django.http import HttpResponse
from django.shortcuts import redirect, render, render_to_response
from django.template.context import RequestContext
from django.views.generic import View


class IndexView(View):
    """View to retrieve companies list."""

    template = 'manager/index.html'

    def get(self, request):
        """Retrieve the list of the all companies."""
        page = request.GET.get('page', 1)
        portal = request.GET.get('portal', None)

        if portal is None:
            response = render(request, self.template, None)
            return response

        context = {}

        context['empresas'] = ['a','b']
        context['total_empresas'] = 100

        response = render(request, self.template, context)
        return response

