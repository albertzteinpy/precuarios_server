# -*- coding: utf-8 -*-


from django.http import HttpResponse
from django.shortcuts import redirect, render, render_to_response
from django.template.context import RequestContext
from django.views.generic import View
from pecuarios.forms import * 
from manager.models import * 
from django.contrib.sites.models import Site
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
import os
from django.template.defaultfilters import slugify
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.forms.models import model_to_dict
from django.contrib.auth.models import User
import re






class EmpresasView(View):
    """View to retrieve companies list."""

    template = 'manager/magazine/empresas.html'

    def get(self, request):
        """Retrieve the list of the all companies."""
        page = request.GET.get('page', 1)
        portal = request.GET.get('portal', None)

        context = {}
        empresas = Empresa.objects.all()
        context['empresas'] = empresas

        response = render(request, self.template, context)
        return response

EmpresasView = EmpresasView.as_view()


class getEmpresasView(View):

    def post(self,request):
        victim = request.POST.get('pk',None)
        a = Empresa.objects.get(pk=victim)
        
        if a.fotoprincipal:
            fieltokill = '%s/%s'%(settings.BASE_DIR,a.fotoprincipal)
            os.remove(fieltokill)            
        a.delete()
        return HttpResponse(a)

    def get(self, request):
        #page = request.GET.get('page', 1)
        #portal = request.GET.get('portal', None)
        context = {}
        empresas = Empresa.objects.all()
        arts = [{
                'pk':x.pk,
                'title':x.nombre_autor,
                'descp':x.desc_usuario
                }
                for x in empresas ]

        response = simplejson.dumps(arts)
        response = HttpResponse(response)
        
        return response

getEmpresasView = getEmpresasView.as_view()



class FormEmpresaView(View):
    template = 'manager/magazine/form_empresa.html'
    def get(self, request):
        #sites = Site.objects.all()

        ida = request.GET.get('id',None)
        if ida:
            instanced = Empresa.objects.get(pk=ida)
        else:
            instanced = None


        #page = request.GET.get('page', 1)
        #portal = request.GET.get('portal', None)
        widgets = {
            'slug_nombre_autor':forms.HiddenInput(),
            'tipo':forms.HiddenInput(),
            'fotoprincipal':forms.HiddenInput(),
            'redes':forms.HiddenInput(),
            #'privado':forms.RadioSelect(attrs={'class':'md-check'}),
        }


        widgetscontactos = {
            'empresapk':forms.HiddenInput(),
        }


        f =  FormCreator()

        forma = f.advanced_form_to_model(modelo=Empresa,
                                         excludes={'vistas','votacion','numvotos','info','slug_empresa','privado'},
                                         widgets=widgets)
        


        for x,y in forma.base_fields.iteritems():
            #y.widget_attrs={'class':'monolito'}
            (y.widget.attrs.update({'class':'form-control'}))
        forma = forma(instance=instanced)        
        context = {}
        context['redsocial'] = [
            {'pk':'facebook','name':'Facebook'},
            {'pk':'twitter','name':'twitter'},
            {'pk':'vimeo','name':'vimeo'},
        ]
        context['forma']=forma
        context['id']=ida
        if instanced:
            context['redes_empresa']=simplejson.dumps(instanced.redes)

        response = render(request, self.template, context)
        return response

FormEmpresaView = FormEmpresaView.as_view()



class addEmpresaView(View):
    #template = 'manager/magazine/form_autor.html'
    def post(self, request):
        #sites = Site.objects.all()
        data = request.POST.copy()
        data['slug_empresa']=slugify(data['nombre_empresa'])
        ida = request.POST.get('id',None)
        if ida:
            instanced = Empresa.objects.get(pk=ida)
            mesagge = u'El registro se actualizó con éxito.'

        else:
            instanced = None
            mesagge = u'El registro se agregó con éxito.' 


        f =  FormCreator()

        forma = f.advanced_form_to_model(modelo=Empresa,excludes={})
        forma = forma(data,instance=instanced)        
        context = {}

        if forma.is_valid():
            save_reg = forma.save()
            responde = {
                'saved':'ok',
                'datos':{'pk':save_reg.pk,
                         'mesagge':mesagge,
                        },
                'callback':'empresaSaved'
            }
            
            return HttpResponse(simplejson.dumps(responde))
        else:
            returner = {'errors':forma.errors}
            return HttpResponse(simplejson.dumps(returner))
        
        response = HttpResponse({'algo sa':'lio mal'})


addEmpresaView = addEmpresaView.as_view()