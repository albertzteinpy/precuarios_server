# -*- coding: utf-8 -*-


from django.http import HttpResponse
from django.shortcuts import redirect, render, render_to_response
from django.template.context import RequestContext
from django.views.generic import View
from pecuarios.forms import * 
from manager.models import * 
from django.contrib.sites.models import Site
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
import os
from django.template.defaultfilters import slugify
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.forms.models import model_to_dict
from django.contrib.auth.models import User
import re
import time
from oauth2client.service_account import ServiceAccountCredentials

#notasecret

# The scope for the OAuth2 request.
SCOPE = 'https://www.googleapis.com/auth/analytics.readonly'

# The location of the key file with the key data.
KEY_FILEPATH = '/home/fireman/attak/d19/pecuarios/pecuarioslocal-72babac251cc.json'

# Defines a method to get an access token from the ServiceAccount object.
def get_access_token():
  return ServiceAccountCredentials.from_json_keyfile_name(
      KEY_FILEPATH, SCOPE).get_access_token().access_token



def parser_stringer(stringToparse,optionvalue=''):
    badcode = '\d+-TRIAL-'
    try:
        return '%s'%(re.sub(badcode,'',stringToparse) if stringToparse else optionvalue)
    except:
        assert False,'error'
        return '%s'%stringToparse


def muser(request):
    u = TblRegs.objects.filter(validado=1)
    a = 1
    for x in u:

        cleanemail = parser_stringer(x.email)
        cleanemail = cleanemail.split(' ')
        username = '%s_%s'%(cleanemail[0].split('@')[0],a)
        a+=1
        passa = 'pecuarios'


        user = User.objects.create_user(username,cleanemail[0],passa)
        user.save()
        x.cve = user
        x.save()
        print username
    return HttpResponse(passa)


class IndexView(View):
    """View to retrieve companies list."""

    template = 'manager/index.html'

    def get(self, request):
        """Retrieve the list of the all companies."""
        page = request.GET.get('page', 1)
        portal = request.GET.get('portal', None)

        if portal is None:
            response = render(request, self.template, None)
            return response

        context = {}

        context['empresas'] = ['a','b']
        context['total_empresas'] = 100

        response = render(request, self.template, context)
        return response

IndexView = IndexView.as_view()



class HomeView(View):
    """View to retrieve companies list."""

    template = 'manager/inicio.html'

    def get(self, request):
        """Retrieve the list of the all companies."""
        #page = request.GET.get('page', 1)
        #portal = request.GET.get('portal', None)
        '''
        if portal is None:
            response = render(request, self.template, None)
            return response

        '''
        context = {}

        context['ACCESS_TOKEN_FROM_SERVICE_ACCOUNT'] = get_access_token()
        context['empresa_slug']=''

        response = render(request, self.template, context)
        return response

HomeView = HomeView.as_view()



class ArtView(View):

    template = 'manager/magazine/articulos.html'
    def get(self, request):
        #page = request.GET.get('page', 1)
        #portal = request.GET.get('portal', None)
        arts = Articulos.objects.all()
        context = {}
        context['articulos'] = arts
        
        response = render(request, self.template, context)
        return response
ArtView = ArtView.as_view()



class getArtView(View):

    def post(self,request):
        victim = request.POST.get('pk',None)
        a = Articulos.objects.get(pk=victim)        
        a.delete()
        return HttpResponse(a)

    def get(self, request):
        #page = request.GET.get('page', 1)
        #portal = request.GET.get('portal', None)
        context = {}
        articulos = Articulos.objects.all()
        arts = [{
                'pk':x.pk,
                'title':x.nombre_articulo,
                'descp':x.texto,
                'votos':x.numvotos
                }
                for x in articulos ]

        response = simplejson.dumps(arts)
        response = HttpResponse(response)
        #context['articulos'] = ['a','b']
        #context['total_empresas'] = 100
        #response = render(request, self.template, context)
        
        return response

getArtView = getArtView.as_view()






#@method_decorator(login_required, name='dispatch')
class FormArtView(View):
    template = 'manager/magazine/form_articulo.html'
    def get(self, request):
        sites = Site.objects.all()

        ida = request.GET.get('id',None)
        if ida:
            instanced = Articulos.objects.get(pk=ida)
        else:
            instanced = None


        #page = request.GET.get('page', 1)
        #portal = request.GET.get('portal', None)
        widgets = {
            'fecha_des_beg':forms.HiddenInput(),
            'fecha_des_end':forms.HiddenInput(),
            'fotoprincipal':forms.HiddenInput(),
            #'portal_articulo':forms.HiddenInput(),
            'descrip':forms.Textarea(
                attrs={
                        'data-provide':'markdown',
                        'class':'md-input'
                        }
            ),
        }

        f =  FormCreator()

        forma = f.advanced_form_to_model(modelo=Articulos,
                                         excludes={'autores','slug','privado'},
                                         widgets=widgets)
        

        for x,y in forma.base_fields.iteritems():
            #y.widget_attrs={'class':'monolito'}
            (y.widget.attrs.update({'class':'form-control'}))
        forma = forma(instance=instanced)        
        context = {}
        context['sites'] = sites
        context['forma']=forma
        context['id']=ida
        context['fotos'] = Galeriaarticulos.objects.filter(idnoticia=ida)

        if instanced:
            context['autores'] = instanced.artautores_set.all().order_by('subindice')

        context['listadoparaautor'] = AUTORES
        response = render(request, self.template, context)
        return response

FormArtView = FormArtView.as_view()


class AddArtView(View):
    #template = 'manager/magazine/form_articulo.html'
    
    def get(self,request):
        return HttpResponse('hollywood')

    def post(self, request):
        ida = request.POST.get('id',None)
        if ida:
            instanced = Articulos.objects.get(pk=ida)
            mesagge = u'El registro se actualizó con éxito.'
        else:
            mesagge = u'El registro se agregó con éxito.' 
            instanced = None

        data = request.POST.copy()
        data['slug']=slugify(data['nombre_articulo'])
        
        try:
            data['slug']=slugify(data['nombre_articulo'])
        except:
            data['slug']=None

        f =  FormCreator()

        forma = f.advanced_form_to_model(modelo=Articulos,excludes={})
        forma = forma(data,instance=instanced)

        if forma.is_valid():
            save_reg = forma.save()
            responde = {
                'saved':'ok',
                'datos':{'pk':save_reg.pk,
                         'mesagge':mesagge,
                        },
                'callback':'articulosaved'
            }
            
            return HttpResponse(simplejson.dumps(responde))
        else:
            returner = {'errors':forma.errors}
            return HttpResponse(simplejson.dumps(returner))
        
        response = HttpResponse({'algo sa':'lio mal'})
        return response

AddArtView = AddArtView.as_view()






class AddAuartView(View):
    #template = 'manager/magazine/form_articulo.html'
    
    def get(self,request):
        return HttpResponse('hollywood')

    def post(self, request):
        mesagge = u'El registro se agregó con éxito.' 
        instanced = None

        data = request.POST.copy()
        
        try:
            data['slug']=slugify(data['nombre_articulo'])
        except:
            data['slug']=None

        f =  FormCreator()

        forma = f.advanced_form_to_model(modelo=ArtAutores,excludes={})
        forma = forma(data,instance=instanced)

        if forma.is_valid():
            save_reg = forma.save()
            responde = {
                'saved':'ok',
                'datos':{'pk':save_reg.pk,
                         'nombre':save_reg.autorpk.nombre_autor,
                         'img':save_reg.autorpk.fotoprincipal,
                         't':save_reg.participacion,
                         's':save_reg.subindice,
                         'mesagge':mesagge,
                        },
                'callback':'autoradded'
            }
            
            return HttpResponse(simplejson.dumps(responde))
        else:
            returner = {'errors':forma.errors}
            return HttpResponse(simplejson.dumps(returner))
        
        response = HttpResponse({'algo sa':'lio mal'})
        return response

AddAuartView = AddAuartView.as_view()




class RmAuartView(View):

    def get(self,request):
        pk = request.GET.get('pk',None)
        if pk:
            instanced = ArtAutores.objects.get(pk=pk)
            instanced.delete()
            return HttpResponse('killed')



        return HttpResponse('hollywood')


RmAuartView = RmAuartView.as_view()






@csrf_exempt
def crop(request):
    tipo = request.POST.get('tipo',1)
    portal = request.POST.get('portal',1)

    uid =slugify(time.time())

#   setting_dirs -----------------------------------------------------
    if not os.path.exists("%s/static/%s/"%(settings.BASE_DIR,portal)):
        os.mkdir("%s/static/%s/"%(settings.BASE_DIR,portal))
    if not os.path.exists("%s/static/%s/%s/"%(settings.BASE_DIR,portal,tipo)):
        os.mkdir("%s/static/%s/%s/"%(settings.BASE_DIR,portal,tipo))
    
#   crop procecing---------------------------------------------------

    #calling libs ---------------------------------------------------
    from PIL import Image
    from io import BytesIO

    #image params --------------------------------------------
    filefi = '%s'%request.POST.get('imgUrl',None)
    
    filefi = filefi.decode('base64')    
    
    #creating a image-----------------------------------------------------
    original = Image.open(BytesIO(filefi))

    #cropping data--------------------------------------------------------
    data = request.POST.copy()
    x1 = int(float(data['imgX1'])) 
    y1 = int(float(data['imgY1']))
    x2 = int(float(data['cropW'])) + x1 
    y2 = int(float(data['cropH'])) + y1
    w = int(float(data['imgW']))
    h = int(float(data['imgH']))
    
    box = (x1,y1,x2,y2)
    resizing = original.resize((w,h),Image.ANTIALIAS)
    cropped = resizing.crop(box)


    nameImage = '%s/static/%s/%s/%s.%s'%(settings.BASE_DIR,portal,tipo,uid,original.format.lower())
    nameimagedb = '/static/%s/%s/%s.%s'%(portal,tipo,uid,original.format.lower())

    
    cropped.save(nameImage,original.format)

    uid = 1
    
    #es.indices.refresh(index='gdata34')
    response = {"status":"success",'uids':uid,
                "url":'%s'%(nameimagedb)}
    response = simplejson.dumps(response)

    return HttpResponse(response)



def addphoto(request):
    args = {}
    context = RequestContext(request)
    forma = FormCreator()
    modelo = Galeriaarticulos
    data = request.POST.copy()
    excludes = {}
    widgets = {}
    forma = forma.advanced_form_to_model(modelo,excludes=excludes,widgets=widgets)
    forma=forma(request.POST,request.FILES)
    forma.logo = request.FILES['foto']
    if forma.is_valid():
        forma.logo = request.FILES['foto']
        saved = forma.save()
        path_response = simplejson.dumps({'path':saved.foto.url})
        return HttpResponse(path_response)
    else:
        assert False,forma.errors
    return render_to_response('admon/index.html',args,context)
