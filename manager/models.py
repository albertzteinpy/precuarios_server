# -*- encoding: utf-8 -*-

from __future__ import unicode_literals

from django.db import models
from django.contrib.sites.models import Site
from django.contrib.auth.models import User
import simplejson


STATUS = (
        ('1','Fuera de línea'),
        ('2','En espera de aprobación'),
        ('3','En línea'),
)


USUARIOS = (
    ('contacto','Contacto'),
    ('autor','Autor'),
)


AUTORES = (
    ('Autor','Autor'),
    ('Co-Autor','Co-Autor'),
    ('Colaborador','Colaborador'),
    ('Fuente','Fuente'),
)

TIPO_PUBLICACION = (
    ('articulo','Artículo'),
    ('noticia','Noticia'),
)



EMPRESAS_NIVEL = (

        ('diamante','Diamante'),
        ('oro','Oro'),
        ('plata','Platino'),
        ('platino','Platino'),
        ('sin nivel','sin nivel'),

    )



SECCIONES = (
    ('home','HOME'),
    ('seccion','SECCIÓN'),
    ('contenido','CONTENIDO'),

    )

TIPO_BANNER = (
    ('728x90','728x90'),
    ('200x200','200x200'),
    ('880x495','880x495'),
    )




TIPO_IMAGEN = (
            ('logo','Logo'),
            ('logo_reporteador','Logo Reporteador'),
            ('bannermicrositio','Baner Micositio'),
            ('imagenmicrositio','Imagen Micrositio'),
            ('pos1','BAN_ALT_POS1'),
            ('pos2','BAN_ALT_POS2'),
            ('pos3','BAN_ALT_POS3'),
            ('pos4','BAN_ALT_POS4'),
            ('pos5','BAN_ALT_POS5'),
            ('pos6','BAN_ALT_POS6'),
            ('pos7','BAN_ALT_POS7'),            
    )


BANNER_VISIBLE = (

    ('web','En la Web'),
    ('movil',u'Móvil'),
    ('ambos',u'Ambos(web y móvil)'),
    )


TIPO_VACANTE = (
    ('solicitante','Solicitante'),
    ('vacante','Vacante'),
    )

TIPOS_PRODUCTO = (

    ('publicacion',u'Publicación'),
    ('producto',u'Producto'),
    )



# --------------------------------------------------------  MODELS PARA LAS SECCIONES
# -----------------------------------------------------------------------------------

class Seccion(models.Model):
    slug = models.CharField(max_length=500)
    fotoprincipal = models.CharField(u'Logo', max_length=255, blank=True, null=True)
    titulo_seccion = models.CharField(u'Nombre Seccion', max_length=255) 
    seccion = models.CharField(u'Sección', max_length=255,choices=SECCIONES) 
    link = models.CharField(u'Link', max_length=500,blank=True,null=True) 
    desc_sec = models.TextField(u'Descripción')
    portal_seccion = models.ForeignKey(Site)    
    fecha_alta = models.DateTimeField(auto_now_add=True)
    fecha_des_beg = models.DateField(u'Fecha destacado inicio',blank=True,null=True)  
    fecha_des_end = models.DateField(u'Fecha destacado final',blank=True,null=True)
    status = models.CharField(u'Estatus',max_length=100,choices=STATUS,default=1)
    vistas = models.DecimalField(u'Visitas',max_digits=18,decimal_places=0,blank=True,null=True)  

    def  __unicode__(self):
        return '%s'%self.titulo_seccion





# ------------------------------------  MODELS PARA LA SECCION DE EMPRESA Y PRODUCTOS
# -----------------------------------------------------------------------------------

class Empresa(models.Model):
    slug_empresa = models.CharField(max_length=500)
    nombre_empresa = models.CharField(u'Nombre Empresa', max_length=255) 
    portal_empresa = models.ForeignKey(Site)
    categoria = models.ForeignKey('CategoriaMagazine',blank=True,null=True)  
    desc_empresa = models.TextField(u'Descripción')
    direcc = models.CharField(u'Dirección', max_length=400)
    cp = models.CharField(u'CP', max_length=10)
    tels = models.CharField(u'Tel', max_length=255, blank=True, null=True)
    fax = models.CharField('FAX', max_length=255, blank=True, null=True)
    web = models.CharField('WEB', max_length=900, blank=True, null=True)
    cve_pais = models.ForeignKey('TblPaises', blank=True, null=True)  
    cve_estado = models.ForeignKey('TblEstados', blank=True, null=True)  
    email = models.CharField(u'email', max_length=6000, blank=True, null=True)
    fotoprincipal = models.CharField(u'Logo', max_length=255, blank=True, null=True)
    fecha_alta = models.DateTimeField(auto_now_add=True)
    status = models.CharField(u'Estatus',max_length=100,choices=STATUS,default=1)
    vistas = models.DecimalField(u'Visitas',max_digits=18,decimal_places=0,blank=True,null=True)  
    votacion = models.DecimalField(u'Votos', max_digits=18, decimal_places=0,blank=True,null=True) 
    numvotos = models.DecimalField(u'num Votos', max_digits=18, decimal_places=0,blank=True,null=True)
    privado = models.BooleanField(u'Privado',default=0)
    redes = models.TextField('Redes',blank=True,null=True)
    nivel_empresa = models.CharField(u'Nivel Empresa',max_length=100,choices=EMPRESAS_NIVEL)
    latitud = models.CharField(u'Latitud', max_length=255, blank=True, null=True)
    longitud = models.CharField(u'Longitud', max_length=255, blank=True, null=True)


    def  __unicode__(self):
        return '%s'%self.nombre_empresa

class Galeriaempresas(models.Model):
    empresa_own = models.ForeignKey(Empresa)
    tipoimagen = models.CharField(u'Tipo',max_length=100,choices=TIPO_IMAGEN)
    imagen = models.ImageField(u'Imagen', max_length=500, blank=True, null=True)
    link = models.CharField(u'Link imagen',max_length=500,blank=True,null=True)

class Contactoempresa(models.Model):
    empresapk = models.ForeignKey(Empresa,verbose_name='Empresa')
    slug = models.CharField(max_length=500)
    nombre_contacto = models.CharField(u'Nombre',max_length=200)
    email_contacto = models.EmailField(u'Correo Electrónico',max_length=500)
    #redes = models.TextField('Redes',blank=True,null=True)
    fotoprincipal = models.CharField(u'Logo', max_length=255, blank=True, null=True)
    cve_pais = models.ForeignKey('TblPaises', blank=True, null=True)  
    cve_estado = models.ForeignKey('TblEstados', blank=True, null=True)  

class Catprods(models.Model):
    slug = models.CharField(max_length=500)
    nombre_categoria = models.CharField(u'Nombre Categoría', max_length=255) 
    parentcat = models.ForeignKey('Catprods',blank=True,null=True) 
    portal = models.ManyToManyField(Site,verbose_name='Portal', blank=True, null=True)  
    fecha_alta = models.DateTimeField('Fecha Alta',auto_now_add=True)  
    imagen = models.ImageField('Imagen',upload_to='static/categoria_productos/', blank=True, null=True)  
    
    def __unicode__(self):
        return self.nombre_categoria


class Producto(models.Model):
    slug = models.CharField(max_length=500)
    nombre = models.CharField('Nombre Producto', max_length=255)  
    portal = models.ForeignKey(Site,verbose_name='Portal')  
    tipo = models.CharField(u'Tipo Producto',max_length=100,choices=TIPOS_PRODUCTO)
    empresapk = models.ForeignKey(Empresa,verbose_name='Empresa', blank=True,null=True)  
    cvecat = models.ManyToManyField('Catprods')  
    tipo = models.IntegerField('TIPO', blank=True, null=True)  
    resenia = models.TextField(u'Reseña', blank=True, null=True)  
    liga = models.CharField('link', max_length=900, blank=True, null=True)  
    fecha_alta = models.DateTimeField('Fecha Alta',auto_now_add=True)  
    status = models.CharField(u'Estatus',default=1,max_length=10,choices=STATUS) 
    email = models.EmailField('Correo Electrónico', max_length=255, blank=True, null=True)  
    descrip = models.TextField('Descripción')  
    vistas = models.IntegerField(u'Visitas',default=0, blank=True, null=True)  
    vistasapp = models.IntegerField(u'Visitas',default=0, blank=True, null=True)  
    votacion = models.DecimalField(u'Votos', max_digits=18, decimal_places=0,default=0, blank=True, null=True)  
    numvotos = models.IntegerField(u'num Votos',default=0,blank=True, null=True) 
    fotoprincipal = models.CharField(u'Foto principal',max_length=600)
    privado = models.BooleanField(u'Contenidop Privado',default=0)
    likes = models.IntegerField(u'likes',default=0)

class Prodcontacto(models.Model):
    productopk=models.ForeignKey(Producto)
    contactopk = models.ForeignKey(Contactoempresa)

class Likeproducto(models.Model):
    productopk  = models.ForeignKey(Producto)
    usuarioreg = models.ForeignKey('TblRegs',blank=True, null=True)
    fecha = models.DateTimeField('fecha',auto_now_add=True)  



# ------------------------------------  MODELS PARA LA SECCION DE MAGAZINE 
# ------------------------------------------------------------------------

class CategoriaMagazine(models.Model):
    nombre_categoria = models.CharField(u'categoría',max_length=255)
    fechapub = models.DateTimeField(auto_now_add=True)
    portal = models.ManyToManyField(Site)
    actv = models.BooleanField(u'Activado',default=1)

    def __unicode__(self):
        return '%s'%self.nombre_categoria


class Articulos(models.Model):
    slug = models.CharField(max_length=500)
    nombre_articulo = models.CharField(u'Título', max_length=500)
    fechapub = models.DateTimeField(u'Fecha de Publicación')
    portal_articulo = models.ForeignKey(Site,verbose_name='Portal')
    cve_empresa = models.ForeignKey(Empresa,verbose_name='Empresa', blank=True,null=True)
    cve_cat = models.ManyToManyField(CategoriaMagazine,verbose_name=u'Categoría')
    texto = models.TextField(u'Texto Corto',blank=True,null=True)
    descrip = models.TextField(u'Contenido')
    fecha_alta = models.DateTimeField(u'Fecha de alta',auto_now_add=True)  
    fecha_des_beg = models.DateField(u'Fecha destacado inicio',blank=True,null=True)  
    fecha_des_end = models.DateField(u'Fecha destacado final',blank=True,null=True)
    status = models.CharField(u'Estatus',default=1,max_length=10,choices=STATUS) 
    tipo = models.CharField(u'Tipo Publicación',max_length=100,choices=TIPO_PUBLICACION)  
    vistas = models.IntegerField(u'Visitas',default=0)  
    vistasapp = models.IntegerField(u'Visitas',default=0)  
    votacion = models.DecimalField(u'Votos', max_digits=18, decimal_places=0,default=0)  
    numvotos = models.IntegerField(u'num Votos',default=0) 
    fotoprincipal = models.CharField(u'Foto principal',max_length=600)
    privado = models.BooleanField(u'Contenidop Privado',default=0)
    likes = models.IntegerField(u'likes',default=0)


    def __unicode__(self):
        return '%s'%self.nombre_articulo
    

    class Meta:
        ordering = ['fechapub']


class Galeriaarticulos(models.Model):
    foto = models.ImageField(u'Logotipo',upload_to = 'static/galeria')
    idnoticia = models.CharField(max_length=200,blank=True,null=True)


class Artscomentarios(models.Model):
    articulopk = models.ForeignKey(Articulos)
    usuariocomemnta = models.ForeignKey('TblRegs', blank=True, null=True)
    comentario = models.TextField(u'Comentario')
    autorizado = models.BooleanField(default=0)  
    fecha_alta = models.DateTimeField('FECHA_ALTA',auto_now_add=True)


class Likearticulos(models.Model):
    articulopk = models.ForeignKey(Articulos)
    usuarioreg = models.ForeignKey('TblRegs',blank=True, null=True)
    fecha = models.DateTimeField('fecha',auto_now_add=True)  


class Autores(models.Model):
    slug_nombre_autor = models.CharField(max_length=500)
    nombre_autor = models.CharField(u'Nombre',max_length=255)
    email = models.EmailField(u'Email',max_length=255)
    redes = models.TextField('Redes',blank=True,null=True)
    info = models.TextField(blank=True,null=True)
    vistas = models.DecimalField(u'Visitas',max_digits=18,decimal_places=0,blank=True,null=True)
    votacion = models.DecimalField(u'Votos', max_digits=18, decimal_places=0,blank=True,null=True)
    numvotos = models.DecimalField(u'num Votos', max_digits=18, decimal_places=0,blank=True,null=True)
    tipo = models.CharField(u'Tipo autor',max_length=100,choices=USUARIOS,default='autor')
    desc_usuario = models.TextField(u'Descripción',blank=True,null=True)
    empresarel = models.ForeignKey(Empresa,blank=True,null=True)
    privado = models.BooleanField(u'Privado',default=0)
    fotoprincipal = models.CharField(u'Foto principal',max_length=600,blank=True,null=True)


class ArtAutores(models.Model):
    autorpk = models.ForeignKey(Autores)
    articulopk = models.ForeignKey(Articulos)
    subindice = models.IntegerField(u'subindice',default=0)
    orden = models.IntegerField(u'Orden',default=0)
    participacion = models.CharField(u'Participación',max_length=200, choices=AUTORES)
    class Meta:
        ordering = ['orden']

class Capsulas(models.Model):
    nombre_capsula = models.CharField(u'NOMBRE', max_length=255)
    portal = models.ForeignKey(Site)  
    descrip = models.TextField('Descripción')  
    texto = models.TextField('Texto')  
    fotoprincipal = models.CharField(u'Foto principal',max_length=600)
    fechapub = models.DateTimeField('Fecha Publicación',auto_now_add=True)
    fecha_des_beg = models.DateField(u'Fecha destacado inicio',blank=True,null=True)  
    fecha_des_end = models.DateField(u'Fecha destacado final',blank=True,null=True)     
    video = models.CharField('VIDEO', max_length=255, blank=True, null=True) 
    fecha_alta = models.DateTimeField('Fecha Alta',auto_now_add=True)  
    status = models.CharField(u'Estatus',default=1,max_length=10,choices=STATUS) 
    posicion_video = models.IntegerField('POSICION_VIDEO', blank=True, null=True)
    publicar = models.CharField('Publicado en',max_length=200,choices=BANNER_VISIBLE)
    patrocinador = models.ForeignKey(Empresa,blank=True,null=True)
    vistas = models.IntegerField(u'Visitas',default=0,blank=True,null=True)  
    vistasapp = models.IntegerField(u'Visitas app',default=0,blank=True,null=True)  
    votacion = models.DecimalField(u'Votos', max_digits=18, decimal_places=0,default=0,blank=True,null=True)  
    numvotos = models.IntegerField(u'num Votos',default=0,blank=True,null=True) 
    privado = models.BooleanField(u'Contenidop Privado',default=0)
    likes = models.IntegerField(u'Likes',default=0)


class Likecapsulas(models.Model):
    capsulapk  = models.ForeignKey(Capsulas)
    usuarioreg = models.ForeignKey('TblRegs',blank=True, null=True)
    fecha = models.DateTimeField('fecha',auto_now_add=True)  


class Videos(models.Model):
    titulo_video = models.CharField(u'Titulo', max_length=255)
    video = models.CharField('URL', max_length=255, blank=True, null=True) 
    vimeominiatura = models.CharField('miniatura', max_length=700, blank=True, null=True) 
    fechapub = models.DateTimeField('Fecha Publicación') 
    fecha_alta = models.DateTimeField('Fecha Alta',auto_now_add=True)  
    portal = models.ForeignKey(Site)  
    descrip = models.TextField('Descripción')  
    cve_cat = models.ManyToManyField(CategoriaMagazine,verbose_name=u'Categoría')
    status = models.CharField(u'Estatus',default=1,max_length=10,choices=STATUS) 
    publicar = models.CharField('Publicado en',max_length=200,choices=BANNER_VISIBLE)
    empresa = models.ForeignKey(Empresa,blank=True,null=True)
    vistas = models.IntegerField(u'Visitas',default=0,blank=True,null=True)  
    vistasapp = models.IntegerField(u'Visitas',default=0,blank=True,null=True)  
    votacion = models.DecimalField(u'Votos', max_digits=18, decimal_places=0,default=0,blank=True,null=True)  
    numvotos = models.IntegerField(u'num Votos',default=0,blank=True,null=True) 
    privado = models.BooleanField(u'Contenidop Privado',default=0)
    likes = models.IntegerField(u'likes',default=0,blank=True,null=True)


class Likevideos(models.Model):
    videopk = models.ForeignKey(Capsulas)
    usuarioreg = models.ForeignKey('TblRegs',blank=True, null=True)
    fecha = models.DateTimeField('fecha',auto_now_add=True)  

class TblJimlong(models.Model):
    nombre = models.CharField('Nombre', max_length=255)  
    portal = models.ForeignKey(Site,verbose_name='Portal')  
    empresapk = models.ForeignKey(Empresa,verbose_name='Empresa', blank=True, null=True)  
    texto = models.TextField('Texto')  
    fecha = models.DateTimeField('FECHA', blank=True, null=True)  
    fecha_alta = models.DateTimeField('Fecha Alta',auto_now_add=True)  
    status = models.CharField(u'Estatus',default=1,max_length=10,choices=STATUS) 
    visible = models.CharField(u'visible',max_length=50,choices=BANNER_VISIBLE)

class TblJimimgs(models.Model):
    cve_jim = models.ForeignKey('TblJimlong')  
    cve_img = models.ImageField(u'',upload_to='static/jimlong/', blank=True, null=True)  


 


#-----------------------------------------MODELOS PARA LOS BANNERS
#-----------------------------------------------------------------



class Banner(models.Model):
    nombre = models.CharField(u'Nombre', max_length=255, blank=True, null=True)  
    portal = models.ForeignKey(Site)  
    imagencuadrado = models.ImageField('Imagen Cuadrado',upload_to='static/banners')  
    imagenlargo = models.ImageField('Imagen Largo',upload_to='static/banners')  
    liga = models.CharField('Liga', max_length=900, blank=True, null=True)  
    fecha_inicio = models.DateField('fecha_inicio')
    fecha_fin = models.DateField('fecha fin', blank=True, null=True)  
    seccion = models.CharField(u'Sección',max_length=100,choices=SECCIONES)
    fecha_alta = models.DateTimeField('Fecha Alta',auto_now_add=True)  
    status = models.CharField(u'Estatus',default=1,max_length=10,choices=STATUS) 
    empresapk = models.ForeignKey('Empresa',verbose_name='Empresa',blank=True,null=True)
    plan = models.CharField(u'Plan contratado',max_length=100,choices=EMPRESAS_NIVEL)
    contador = models.IntegerField(default=0)  
    clicks = models.IntegerField(u'clicks',default=0)
    impresiones = models.IntegerField(u'impreciones',default=0)
    posicion = models.IntegerField(u'impreciones',default=0)
    
    visible = models.CharField(u'visible',max_length=50,choices=BANNER_VISIBLE)

    def __unicode__(self):
        return '%s %s'%(self.nombre,self.contador)

'''
class TblBannersPatrocinadores(models.Model):
    nombre = models.CharField('Nombre', max_length=100, blank=True, null=True) 
    cve_empresa = models.ForeignKey('Empresa',blank=True, null=True) 
    portal = models.ForeignKey(Site,blank=True, null=True) 
    imagen = models.ImageField(u'Imagen',upload_to='static/bannerpatrocinador',blank=True, null=True) 
    status = models.SmallIntegerField('STATUS', blank=True, null=True) 
    impresiones = models.IntegerField('IMPRESIONES',default=0) 
'''



# -------------------------------------------MODELO PARA USUARIOS REGISTRADOS POR PORTAL
#---------------------------------------------------------------------------------------

class TblPaises(models.Model):
    nombre = models.CharField('NOMBRE', max_length=255, blank=True, null=True)
    status = models.SmallIntegerField('STATUS', blank=True, null=True)  
    bandera = models.CharField('BANDERA', max_length=255, blank=True, null=True)  

    def __unicode__(self):
        return '%s'%self.nombre

class TblEstados(models.Model):
    nombre = models.CharField('NOMBRE', max_length=255, blank=True, null=True)  
    cve_pais = models.ForeignKey('TblPaises', models.DO_NOTHING, blank=True, null=True)  
    status = models.SmallIntegerField('STATUS', blank=True, null=True)  
    def __unicode__(self):
        return '%s'%self.nombre

class TblRegs(models.Model):
    cve = models.ForeignKey(User,blank=True,null=True)
    nom = models.CharField('Nombre', max_length=100)  
    apa = models.CharField('A. Paterno', max_length=100)
    ama = models.CharField('A. Materno', max_length=100)
    email = models.CharField('EMAIL', max_length=100, blank=True, null=True)      
    fecnac = models.DateTimeField('Fecha Nacimieno', blank=True, null=True)
    pswd = models.CharField('PSWD', max_length=20, blank=True, null=True)
    cve_pais = models.ForeignKey(TblPaises, blank=True, null=True)  
    cve_estado = models.ForeignKey(TblEstados, blank=True, null=True)  
    ocupacion = models.IntegerField('OCUPACION', blank=True, null=True)  
    tipoprod = models.IntegerField('TIPOPROD', blank=True, null=True)  
    subtipoprod = models.IntegerField('SUBTIPOPROD', blank=True, null=True) 
    numreprod = models.IntegerField('NUMREPROD', blank=True, null=True)  
    recibenl = models.SmallIntegerField('RECIBENL', blank=True, null=True) 
    fecha_alta = models.DateTimeField('FECHA_ALTA', blank=True, null=True) 
    status = models.SmallIntegerField('STATUS', blank=True, null=True)  
    portal = models.CharField('Sitio',max_length=20, blank=True, null=True)  
    validado = models.SmallIntegerField('VALIDADO', blank=True, null=True)
    cve_empresa = models.CharField(max_length=100, blank=True, null=True)  
    numaccesos = models.DecimalField(max_digits=18, decimal_places=0, blank=True, null=True) 
    token = models.CharField('TOKEN', max_length=2000, blank=True, null=True) 

# -------------------------------------------MODELOS PARA BOLSA DE TRABAJO VACANTES Y SOLICITANTES
#-------------------------------------------------------------------------------------------------


class Bolsa(models.Model):
    titulo = models.CharField('Nombre', max_length=255)  
    profesion = models.CharField(u'Profesión',max_length=500,blank=True, null=True)  
    portal = models.ForeignKey(Site)    
    cve_pais = models.ForeignKey('TblPaises')  
    cve_estado = models.ForeignKey('TblEstados')  
    tipo = models.CharField('Tipo anuncio',choices=TIPO_VACANTE,max_length=100, blank=True, null=True)  
    texto = models.CharField('TEXTO', max_length=8000, blank=True, null=True)  
    imagen = models.ImageField('Imagen',upload_to='static/bolsa/',blank=True, null=True)  
    fechavence = models.DateTimeField('Fecha Vence', blank=True, null=True)  
    fecha_alta = models.DateTimeField('Fecha Alta',auto_now_add=True) 
    fechamod = models.DateField('Fecha Modificación',auto_now=True) 
    status = models.CharField(u'Estatus',default=1,max_length=10,choices=STATUS) 
    vistas = models.DecimalField('VISTAS', max_digits=18, decimal_places=0, blank=True, null=True)  
    likes = models.IntegerField(u'likes',default=0)
    interesados = models.IntegerField(u'interesados',default=0)


class Bolsaempresarel(models.Model):
    bolsapk = models.ForeignKey(Bolsa)
    contacto = models.ForeignKey(Contactoempresa)


class Bolsasolicitante(models.Model):
    bolsapk = models.ForeignKey(Bolsa)
    publicadopor = models.ForeignKey(TblRegs)

class Bolsafiles(models.Model):
    bolsapk = models.ForeignKey('Bolsa')  
    archivo = models.FileField('Archivo',upload_to='static/bolsa/')  
    fecha_alta = models.DateTimeField('FECHA_ALTA', blank=True, null=True)  


class Bolsamsg(models.Model):
    cve_bolsa = models.ForeignKey(Bolsa)  
    cve_reg = models.ForeignKey(TblRegs)  
    mensaje = models.TextField('MENSAJE', blank=True, null=True)  
    fecha_alta = models.DateTimeField('FECHA_ALTA', blank=True, null=True)  
    archivo = models.FileField('Archivo',upload_to='static/bolsa/uploads/', blank=True, null=True)  

class Bolsavistas(models.Model):
    cve_bolsa = models.ForeignKey(Bolsa)  
    cve_reg = models.ForeignKey(TblRegs)  
    fecha = models.DateTimeField('FECHA',auto_now_add=True)  
    visitaapp = models.IntegerField(blank=True, null=True)

class Bolsalikes(models.Model):
    cve_bolsa = models.ForeignKey(Bolsa)  
    cve_reg = models.ForeignKey(TblRegs)  
    fecha = models.DateTimeField('FECHA',auto_now_add=True)  
    visitaapp = models.IntegerField(blank=True, null=True)

#-------------------------------------------------------------MODELOS PARA EVENTOS
#----------------------------------------------------------------------------------

class Eventos(models.Model):
    nombre_evento = models.CharField(u'Nombre del Evento', max_length=255) 
    slug = models.CharField(max_length=500)
    portal = models.ForeignKey(Site)  
    cve_pais = models.ForeignKey(TblPaises) 
    cve_estado = models.ForeignKey(TblEstados)  
    ciudad = models.CharField('Ciudad', max_length=255)  
    programa = models.TextField(u'Programa')  
    resenia = models.TextField(u'Reseña', blank=True, null=True)  
    fecini = models.DateField('Fecha Inicio')  
    fecfin = models.DateField('Fecha Fin')  
    logo = models.ImageField('Logo',upload_to='static/eventos/', blank=True, null=True)  
    logopec = models.ImageField('LogoPec', upload_to='static/eventos', blank=True, null=True)  
    fecha_alta = models.DateTimeField('fecha_alta',auto_now_add=True)  
    descrip = models.TextField(u'Descripción', blank=True, null=True)  
    imgprog = models.ImageField('Imagen Programa',upload_to='static/eventos/', blank=True, null=True)  
    cve_empresa = models.ForeignKey(Empresa,verbose_name='Empresa Relacionada', blank=True, null=True)  
    datoscontacto = models.TextField(u'Datos Contacto', blank=True, null=True)  
    video = models.CharField(u'Video',max_length=255, blank=True, null=True)
    latitud = models.CharField('latitud', max_length=50, blank=True, null=True)  
    longitud = models.CharField('longitud', max_length=50, blank=True, null=True)  
    vistas = models.IntegerField(u'Visitas',default=0)  
    vistasapp = models.IntegerField(u'Visitas',default=0)  
    votacion = models.DecimalField(u'Votos', max_digits=18, decimal_places=0,default=0)  
    numvotos = models.IntegerField(u'num Votos',default=0) 
    likes = models.IntegerField(u'likes',default=0)

class Likeeventos(models.Model):
    videopk  = models.ForeignKey(Eventos)
    usuarioreg = models.ForeignKey('TblRegs')
    fecha = models.DateTimeField('fecha',auto_now_add=True)  


class TblEvesimgs(models.Model):
    cve_evento = models.ForeignKey(Eventos)
    cve_img = models.ImageField(u'Imagen',upload_to='static/eventos/uploads/')

class TblExpositores(models.Model):
    nombre = models.CharField('Nombre', max_length=255) 
    cv = models.TextField('CV', blank=True, null=True) 
    email = models.EmailField(u'EMAIL', max_length=500) 
    tels = models.CharField(u'Tels.', max_length=255, blank=True, null=True) 
    fotoprincipal = models.CharField(u'Foto principal',max_length=600)
    fecha_alta = models.DateTimeField('FECHA_ALTA',auto_now_add=True) 
    status = models.CharField(u'Estatus',default=1,max_length=10,choices=STATUS) 
    url = models.CharField('URL', max_length=511, blank=True, null=True) 
    vistas = models.IntegerField(u'Visitas',default=0)  
    vistasapp = models.IntegerField(u'Visitas',default=0)  
    votacion = models.DecimalField(u'Votos', max_digits=18, decimal_places=0,default=0)  
    numvotos = models.IntegerField(u'num Votos',default=0) 
    likes = models.IntegerField(u'likes',default=0)


#-------------------------------------------------------------MODELOS PARA EVENTOS
#----------------------------------------------------------------------------------


class Clasificados(models.Model):
    titulo = models.CharField(u'Título', max_length=255)
    slug = models.CharField(max_length=500)
    portal = models.IntegerField(Site)  
    cve_reg = models.ForeignKey('TblRegs')  
    cve_pais = models.ForeignKey('TblPaises')  
    cve_estado = models.ForeignKey('TblEstados')  
    texto = models.TextField('Texto')  
    fechavence = models.DateTimeField('Fecha vence')  
    fecha_alta = models.DateTimeField('FECHA_ALTA',auto_now_add=True)  
    precio = models.FloatField('PRECIO', blank=True, null=True)
    status = models.CharField(u'Estatus',default=1,max_length=10,choices=STATUS) 
    vistas = models.IntegerField(u'Visitas',default=0)  
    vistasapp = models.IntegerField(u'Visitas',default=0)  
    votacion = models.DecimalField(u'Votos', max_digits=18, decimal_places=0,default=0)  
    numvotos = models.IntegerField(u'num Votos',default=0) 
    fotoprincipal = models.CharField(u'Foto principal',max_length=600)
    privado = models.BooleanField(u'Contenidop Privado',default=0)
    likes = models.IntegerField(u'likes',default=0)

class Galeriaclasificados(models.Model):
    foto = models.ImageField(u'Imagen',upload_to = 'static/clasificados/')
    clasificadopk  = models.ForeignKey(Clasificados)

class Clasificadosmsg(models.Model):
    cve_clasif = models.ForeignKey(Clasificados)  
    cve_reg = models.ForeignKey('TblRegs')  
    mensaje = models.TextField(u'Mensaje', blank=True, null=True)  
    fecha_alta = models.DateTimeField(u'FECHA_ALTA',auto_now_add=True)  
    status = models.CharField(u'Estatus',max_length=10,choices=STATUS,default='2') 

class Clasificadolike(models.Model):
    cve_clasif = models.ForeignKey(Clasificados)  
    cve_reg = models.ForeignKey('TblRegs')  
    fecha_alta = models.DateTimeField(u'FECHA_ALTA',auto_now_add=True)  


#-----------------------------------------------------------MODELOS PARA ENCUESTAS
#----------------------------------------------------------------------------------

class Encuestas(models.Model):
    nombre = models.CharField(u'NOMBRE', max_length=255, blank=True, null=True)  
    portal = models.ForeignKey(Site)  
    cve_empresa = models.ForeignKey(Empresa,blank=True,null=True)  
    fecha_alta = models.DateTimeField(u'FECHA_ALTA',auto_now_add=True)
    desde = models.DateTimeField(u'Desde', blank=True, null=True)  
    hasta = models.DateTimeField(u'Hasta', blank=True, null=True)  
    fechas = models.TextField(u'Fechas', blank=True, null=True)  
    status = models.CharField(u'Estatus',default=1,max_length=10,choices=STATUS) 
    vistas = models.IntegerField(u'Visitas',default=0)  
    vistasapp = models.IntegerField(u'Visitas',default=0)  
    votacion = models.DecimalField(u'Votos', max_digits=18, decimal_places=0,default=0)  
    numvotos = models.IntegerField(u'num Votos',default=0) 
    privado = models.BooleanField(u'Contenidop Privado',default=0)
    likes = models.IntegerField(u'likes',default=0)

class Preguntaencuesta(models.Model):
    encuestapk = models.ForeignKey(Encuestas)
    pregunta = models.CharField(u'Pregunta',max_length=500)

class Opcionpregunta(models.Model):
    preguntapk = models.ForeignKey(Preguntaencuesta)
    opcion = models.CharField(u'Opción',max_length=500)
    numvotos = models.IntegerField(u'num Votos',default=0) 

class Usresponde(models.Model):
    encunestapk = models.ForeignKey(Encuestas)
    usuarioreg = models.ForeignKey('TblRegs')


#---------------------------------------------------------------MODELOS PARA FOROS
#----------------------------------------------------------------------------------

class Forocat(models.Model):
    nombre_categoria = models.CharField(u'categoría',max_length=255)
    fechapub = models.DateTimeField(auto_now_add=True)
    portal = models.ManyToManyField(Site)
    actv = models.BooleanField(u'Activado',default=1)

    def __unicode__(self):
        return '%s'%self.nombre_categoria

class Foros(models.Model):
    nombre = models.CharField(u'Nombre',max_length=500)  
    portal = models.IntegerField(Site)  
    categoria = models.ForeignKey(Forocat)  
    cve_pais = models.ForeignKey(TblPaises)  
    cve_estado = models.ForeignKey(TblEstados)  
    ciudad = models.CharField(u'Ciudad', max_length=255, blank=True, null=True)  
    autor = models.ForeignKey(TblRegs)  
    texto = models.TextField(u'Texto')  
    fecha_alta = models.DateTimeField(u'FECHA_ALTA',auto_now_add=True)  
    status = models.CharField(u'Estatus',default=1,max_length=10,choices=STATUS) 
    vistas = models.IntegerField(u'Visitas',default=0)  
    vistasapp = models.IntegerField(u'Visitas',default=0)  
    votacion = models.DecimalField(u'Votos', max_digits=18, decimal_places=0,default=0)  
    numvotos = models.IntegerField(u'num Votos',default=0) 
    privado = models.BooleanField(u'Contenidop Privado',default=0)
    likes = models.IntegerField(u'likes',default=0)

class Foromensajes(models.Model):
    cve_foro = models.ForeignKey('Foros')  
    cve_reg = models.ForeignKey('TblRegs')  
    texto = models.TextField(u'Texto', blank=True, null=True)  
    autorizado = models.IntegerField(u'AUTORIZADO', blank=True, null=True)  
    fecha_alta = models.DateTimeField(u'FECHA_ALTA', auto_now_add=True)  
    status = models.CharField(u'Estatus',default=1,max_length=10,choices=STATUS) 



#--------------------------------------------------------------------OTROS MODELOS
#----------------------------------------------------------------------------------


class TblPush(models.Model):
    cve_reg = models.ForeignKey('TblRegs')  
    portal = models.ForeignKey(Site)  
    nombre = models.CharField(u'Nombre',max_length=255)  
    cve_pais = models.ForeignKey(TblPaises)  
    cve_estado = models.ForeignKey(TblEstados)  
    lada = models.CharField(u'Lada', max_length=10, blank=True, null=True)  
    tel = models.CharField(u'Tel', max_length=50, blank=True, null=True)  
    email = models.CharField(u'eMail', max_length=100, blank=True, null=True)  
    medio = models.SmallIntegerField(u'Medio', blank=True, null=True)  
    dia = models.SmallIntegerField(u'Día', blank=True, null=True)  
    hora = models.SmallIntegerField(u'Hora', blank=True, null=True)  
    bandera = models.SmallIntegerField(u'Bandera', blank=True, null=True)  
    fecha_alta = models.DateTimeField(u'FECHA_ALTA',auto_now_add=True)  
    status = models.CharField(u'Estatus',default=1,max_length=10,choices=STATUS) 
    fecha_contacto = models.DateTimeField(u'Fecha Contácto', blank=True, null=True)  
    tipotel = models.SmallIntegerField(u'Tipo Tel.', blank=True, null=True)  
    empresa = models.ForeignKey(Empresa, blank=True, null=True)  

class Razas(models.Model):
    nombre = models.CharField(u'NOMBRE', max_length=255)  
    portal = models.ForeignKey(Site)  
    cve_empresa = models.ForeignKey(Empresa,blank=True,null=True)  
    texto = models.TextField(u'Texto', blank=True, null=True)  
    fecha_alta = models.DateTimeField(u'FECHA_ALTA',auto_now_add=True)  
    status = models.CharField(u'Estatus',default=1,max_length=10,choices=STATUS) 
    descrip = models.TextField(u'Descripción')  

class TblRazasimgs(models.Model):
    cve_raza = models.ForeignKey(Razas)  
    imagen = models.ImageField(u'imagen',upload_to='static/razas/')  

class Tips(models.Model):
    nombre = models.CharField(u'Nombre', max_length=255)  
    portal = models.ForeignKey(Site)  
    descrip = models.TextField(u'Descripción', blank=True, null=True)  
    texto = models.TextField(u'Texto', blank=True, null=True)  
    fechapub = models.DateTimeField(u'FEcha Publicación')  
    video = models.CharField(u'Video', max_length=255, blank=True, null=True)  
    fecha_alta = models.DateTimeField(u'FECHA_ALTA',auto_now_add=True)  
    status = models.CharField(u'Estatus',default=1,max_length=10,choices=STATUS) 
    vistas = models.IntegerField(u'Visitas',default=0)  
    vistasapp = models.IntegerField(u'Visitas',default=0)  
    votacion = models.DecimalField(u'Votos', max_digits=18, decimal_places=0,default=0)  
    numvotos = models.IntegerField(u'num Votos',default=0) 
    fotoprincipal = models.CharField(u'Foto principal',max_length=600)
    privado = models.BooleanField(u'Contenidop Privado',default=0)
    likes = models.IntegerField(u'likes',default=0)

class TblTipscoments(models.Model):
    cve_reg = models.ForeignKey(TblRegs)  
    cve_tip = models.ForeignKey(Tips)  
    texto = models.TextField(u'Texto', blank=True, null=True)  
    fecha_alta = models.DateTimeField(u'FECHA_ALTA',auto_now_add=True)  
    status = models.CharField(u'Estatus',default=1,max_length=10,choices=STATUS) 
    vistas = models.IntegerField(u'Visitas',default=0)  
    vistasapp = models.IntegerField(u'Visitas',default=0)  
    votacion = models.DecimalField(u'Votos', max_digits=18, decimal_places=0,default=0)  
    likes = models.IntegerField(u'likes',default=0)

class TblTipsvotacion(models.Model):
    cve_tip = models.ForeignKey(Tips)  
    cve_reg = models.ForeignKey(TblRegs)  
    valor = models.IntegerField(u'VALOR')  
    fecha = models.DateTimeField(u'FECHA',auto_now_add=True)  

class TblTotstips(models.Model):
    cve_tip = models.ForeignKey(Tips)  
    fecha = models.DateTimeField(u'FECHA', blank=True, null=True)  
    estafirmado = models.IntegerField(u'ESTAFIRMADO', blank=True, null=True)  
    numero = models.DecimalField(u'NUMERO', max_digits=18, decimal_places=0, blank=True, null=True)  

