# -*- coding: utf-8 -*-


from django.http import HttpResponse
from django.shortcuts import redirect, render, render_to_response
from django.template.context import RequestContext
from django.views.generic import View
from pecuarios.forms import * 
from manager.models import * 
from django.contrib.sites.models import Site
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
import os
from django.template.defaultfilters import slugify
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.forms.models import model_to_dict
from django.contrib.auth.models import User
from django.contrib.sites.shortcuts import get_current_site

import re






class SeccionesView(View):
    """View to retrieve companies list."""

    template = 'manager/secciones/listado.html'

    def get(self, request):
        """Retrieve the list of the all companies."""
        seccion = request.GET.get('s', None)
        portal = request.GET.get('p', None)
        if portal:
            portale = Site.objects.get(pk=portal)
        else:
            portale = get_current_site(request)

        context = {}
        widgets = {}

        f =  FormCreator()  

        forma = f.advanced_form_to_model(modelo=Seccion,
                                         excludes=['slug','vistas'],
                                         widgets=widgets)
        for x,y in forma.base_fields.iteritems():
            #y.widget_attrs={'class':'monolito'}
            (y.widget.attrs.update({'class':'form-control'}))


        context['seccion'] = seccion
        context['forma'] = forma
        
        context['current_site'] = portale
        
        context['sites']=Site.objects.all()
        context['currseccion']=Seccion.objects.filter(portal_seccion=portale)

        

        response = render(request, self.template, context)
        return response

SeccionesView = SeccionesView.as_view()


class getEmpresasView(View):

    def post(self,request):
        victim = request.POST.get('pk',None)
        a = Empresa.objects.get(pk=victim)
        
        if a.fotoprincipal:
            fieltokill = '%s/%s'%(settings.BASE_DIR,a.fotoprincipal)
            os.remove(fieltokill)            
        a.delete()
        return HttpResponse(a)

    def get(self, request):
        #page = request.GET.get('page', 1)
        #portal = request.GET.get('portal', None)
        context = {}
        empresas = Empresa.objects.all()
        arts = [{
                'pk':x.pk,
                'title':x.nombre_autor,
                'descp':x.desc_usuario
                }
                for x in empresas ]

        response = simplejson.dumps(arts)
        response = HttpResponse(response)
        
        return response

getEmpresasView = getEmpresasView.as_view()



class FormSeccionView(View):
    template = 'manager/secciones/formulario.html'
    def get(self, request):
        #sites = Site.objects.all()

        ida = request.GET.get('id',None)
        if ida:
            instanced = Seccion.objects.get(pk=ida)
        else:
            instanced = None


        #page = request.GET.get('page', 1)
        #portal = request.GET.get('portal', None)
        widgets = {
            'slug_nombre_autor':forms.HiddenInput(),
            'tipo':forms.HiddenInput(),
            'fotoprincipal':forms.HiddenInput(),
            'redes':forms.HiddenInput(),
            #'privado':forms.RadioSelect(attrs={'class':'md-check'}),
        }


        widgetscontactos = {
            'empresapk':forms.HiddenInput(),
        }


        f =  FormCreator()

        forma = f.advanced_form_to_model(modelo=Seccion,
                                         excludes={'vistas','votacion','numvotos','info','slug'},
                                         widgets=widgets)
        


        for x,y in forma.base_fields.iteritems():
            #y.widget_attrs={'class':'monolito'}
            (y.widget.attrs.update({'class':'form-control'}))
        forma = forma(instance=instanced)        
        context = {}
        context['forma']=forma
        context['id']=ida

        response = render(request, self.template, context)
        return response

FormSeccionView = FormSeccionView.as_view()



class addSeccionView(View):
    #template = 'manager/magazine/form_autor.html'
    def post(self, request):
        #sites = Site.objects.all()
        data = request.POST.copy()
        data['slug']=slugify(data['titulo_seccion'])
        ida = request.POST.get('id',None)
        if ida:
            instanced = Seccion.objects.get(pk=ida)
            mesagge = u'El registro se actualizó con éxito.'

        else:
            instanced = None
            mesagge = u'El registro se agregó con éxito.' 


        f =  FormCreator()

        forma = f.advanced_form_to_model(modelo=Seccion,excludes={})
        forma = forma(data,instance=instanced)        
        context = {}

        if forma.is_valid():
            save_reg = forma.save()
            responde = {
                'saved':'ok',
                'datos':{'pk':save_reg.pk,
                         'mesagge':mesagge,
                        },
                'callback':'seccionSaved'
            }
            
            return HttpResponse(simplejson.dumps(responde))
        else:
            returner = {'errors':forma.errors}
            return HttpResponse(simplejson.dumps(returner))
        
        response = HttpResponse({'algo sa':'lio mal'})


addSeccionView = addSeccionView.as_view()