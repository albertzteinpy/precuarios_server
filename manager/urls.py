# -*- coding: utf-8 -*-
"""Proyect url's."""

from django.conf.urls import patterns, url


urlpatterns = patterns(
    'manager.magazine_views',
    url(r'^$', 'IndexView', name='inicio'),
    url(r'^home/$', 'HomeView', name='home'),
    url(r'^articulos/$', 'ArtView', name='articulos'),
    url(r'^formarticulo/$', 'FormArtView', name='formarticulo'),
    url(r'^addarticulo/$', 'AddArtView', name='addarticulo'),
    url(r'^addautorart/$', 'AddAuartView', name='addautorart'),
    url(r'^rmautorart/$', 'RmAuartView', name='rmautorart'),
    url(r'^crop/$', 'crop', name='crop'),
    url(r'^getarticulos/$', 'getArtView', name='getarticulos'),
    url(r'^addphoto/$', 'addphoto', name='addphoto'),
    url(r'^muser/$', 'muser', name='muser'),
)



urlpatterns += patterns(
    'manager.autores_views',
    url(r'^autores/$', 'AutoresView', name='autores'),
    url(r'^getautores/$', 'getAutoresView', name='getautores'),
    url(r'^formautor/$', 'FormAutorView', name='formautor'),
    url(r'^addautor/$', 'addAutorView', name='addautor'),

)


urlpatterns += patterns(
    'manager.empresas_views',
    url(r'^empresas/$', 'EmpresasView', name='empresas'),
    url(r'^getempresas/$', 'getEmpresasView', name='getempresas'),
    url(r'^formempresa/$', 'FormEmpresaView', name='forempresa'),
    url(r'^addempresa/$', 'addEmpresaView', name='addempresa'),

)


urlpatterns += patterns(
    'manager.categoria_magazine_views',
    url(r'^categorias/$', 'CatView', name='categorias'),
    url(r'^getcats/$', 'getCatView', name='getcats'),
    url(r'^formcats/$', 'FormCatView', name='formcats'),
    url(r'^addcat/$', 'addCatView', name='addcat'),

)


urlpatterns += patterns(
    'manager.capsulas_views',
    url(r'^capsulas/$', 'CapsulasView', name='capsulas'),
    url(r'^getcapsulas/$', 'getCapsulasView', name='getcapsulas'),
    url(r'^formcapsulas/$', 'FormCapsulasView', name='formcapsulas'),
    url(r'^addcapsulas/$', 'addCapsulaView', name='addcapsulas'),

)


urlpatterns += patterns(
    'manager.videos_views',
    url(r'^videos/$', 'VideosView', name='videos'),
    url(r'^getvideos/$', 'getVideosView', name='getvideos'),
    url(r'^formvideos/$', 'FormVideosView', name='formvideos'),
    url(r'^addvideos/$', 'addVideosView', name='addvideos'),

)


urlpatterns += patterns(
    'manager.jlong_views',
    url(r'^jimlong/$', 'JimlongView', name='jimlong'),
    url(r'^getjimlong/$', 'getJimlongView', name='getjimlong'),
    url(r'^formjimlong/$', 'FormVideosView', name='formjimlong'),
    url(r'^addjimlong/$', 'addJimlongView', name='addjimlong'),

)


#----------------------------------------------__EMPRESAS
#----------------------------------------------CATEGORIAS

urlpatterns += patterns(
    'manager.dempresa.categorias_views',
    url(r'^catempresas/$', 'CatEmView', name='catempresas'),
    url(r'^getcatempresas/$', 'getCatEmView', name='getcatempresas'),
    url(r'^formcatempresas/$', 'FormCatEmView', name='formcatempresas'),
    url(r'^addcatempresas/$', 'addCatEmView', name='addcatempresas'),

)


#------------------------------------------------EMPRESAS
#-----------------------------------------------PRODUCTOS

urlpatterns += patterns(
    'manager.dempresa.productos_views',
    url(r'^productos/$', 'ProductoView', name='productos'),
    url(r'^getproductos/$', 'getProductoView', name='getproductos'),
    url(r'^formproductos/$', 'FormProductoView', name='formproductos'),
    url(r'^addproducto/$', 'addProductoView', name='addproducto'),

)


#-----------------------------------------------------------------EMPRESAS
#---------------------------------------------------------------CONTATCTOS

urlpatterns += patterns(
    'manager.dempresa.contactos_views',
    url(r'^contactos/$', 'ContactoView', name='contactos'),
    url(r'^getcontactos/$', 'getContactoView', name='getcontactos'),
    url(r'^formcontacto/$', 'FormContactoView', name='formcontacto'),
    url(r'^addcontacto/$', 'addContactoView', name='addcontacto'),

)


#-------------------------------------------------------------- BANNERS
#---------------------------------------------------------------BANNERS

urlpatterns += patterns(
    'manager.banners.banners_views',
    url(r'^banners/$', 'BannerView', name='banners'),
    url(r'^getbanners/$', 'getBannerView', name='getbanners'),
    url(r'^formbanners/$', 'FormBannerView', name='formbanners'),
    url(r'^addbanner/$', 'addBannerView', name='addbanner'),

)


#-------------------------------------------------------------- EVENTOS
#---------------------------------------------------------------EVENTOS

urlpatterns += patterns(
    'manager.eventos.eventos_views',
    url(r'^eventos/$', 'EventosView', name='eventos'),
    url(r'^geteventos/$', 'getEventosView', name='geteventos'),
    url(r'^formeventos/$', 'FormEventosView', name='formeventos'),
    url(r'^addevento/$', 'addEventoView', name='addevento'),

)



#-------------------------------------------------------------- SECCIONES
#---------------------------------------------------------------EVENTOS

urlpatterns += patterns(
    'manager.secciones_views',
    url(r'^secciones/$', 'SeccionesView', name='secciones'),
    #url(r'^geteventos/$', 'getEventosView', name='geteventos'),
    url(r'^formsecciones/$', 'FormSeccionView', name='formsecciones'),
    url(r'^addseccion/$', 'addSeccionView', name='addseccion'),

)


