# -*- encoding: utf-8 -*-
from django.http import HttpResponse
from django.template.context import RequestContext
from django.shortcuts import render_to_response, render
from django.contrib.auth.decorators import login_required, permission_required
from django.shortcuts import redirect
import string as STrg
import datetime
import simplejson
from elasticsearch import Elasticsearch
from django.views.decorators.csrf import csrf_exempt
import os
from django.conf import settings
from pecuarios.forms import *
from manager.forms import *

print dir (manager.forms)
@login_required(login_url='/manager')
def empresa_edit(request):
    forma = EmpresaForm

    empresa = request.GET.get('empresa', None)
    es = Elasticsearch(port=9200)
    if empresa:
        data = es.get(index="pecuarios", doc_type='empresas', id=empresa)
    else:
        data = None
    args = {}
    context = RequestContext(request)
    args['data'] = data['_source']
    forma = forma(initial=data['_source'])
    args['forma'] = forma
    args['id'] = empresa
    sa = 'manager/formempresas.html'
    response = render_to_response(sa, args, context)
    return response


@login_required(login_url='/manager')
def saveempresa(request):

    args = {}
    context = RequestContext(request)
    d = {}
    data = request.POST.copy()
    data['cve_empresa'] = 1
    forma = EmpresaForm
    forma = forma(data)

    del data['csrfmiddlewaretoken']
    del data['id']

    if forma.is_valid():
        response = {'si': 'sii'}
        es = Elasticsearch(port=9200)
        res = es.create(index="pecuarios", doc_type='empresas',
                        ignore=400, body=data)

    else:
        response = {'errors': forma.errors}

    return HttpResponse(simplejson.dumps(response))


@login_required(login_url='/manager')
def savepost(request):

    args = {}
    context = RequestContext(request)
    d = {}
    data = request.POST.copy()
    authors = request.POST.getlist('nombre_autor')
    tauthors = request.POST.getlist('tipo_autor')
    superindice = request.POST.getlist('super_indice')

    uid = request.POST.get('uid', None)

    destacadoini = request.POST.get('fecha_des_beg', None)
    destacadofin = request.POST.get('fecha_des_end', None)

    authors = [{'type': x, 'name': authors[
        tauthors.index(x)]} for x in tauthors]
    d['title'] = data['titulo']
    d['extract'] = data['extracto']
    d['description'] = data['contenido']
    d['created_at'] = datetime.datetime.now()
    d['CVE_ART'] = 1
    d['fechapub'] = datetime.datetime.strptime(data['fechapub'], '%m/%d/%Y')
    d['portal'] = 1
    d['tipo'] = 1
    d['cve_empresa'] = 1  # data['titulo']
    d['cve_cat'] = data['catart']
    d['status'] = 0
    d['autor'] = authors
    d['fecha_des_beg'] = destacadoini
    d['fecha_des_end'] = destacadofin

    es = Elasticsearch(port=9200)
    if uid:
        res = es.update(index='pecuarios', doc_type='magazine',
                        id=uid, body={"doc": d})
    else:
        res = es.create(index="pecuarios", doc_type='magazine',
                        ignore=400, body=d)

    return HttpResponse(simplejson.dumps(res))

    sa = 'manager/newpost.html'
    response = render_to_response(sa, args, context)

    return response


@csrf_exempt
def crop(request):
    tipo = 1
    portal = 1
    uid = request.POST.get('uid', None)
#   setting_dirs -----------------------------------------------------
    if not os.path.exists("%s/static/%s/" % (settings.BASE_DIR, portal)):
        os.mkdir("%s/static/%s/" % (settings.BASE_DIR, portal))
    if not os.path.exists("%s/static/%s/%s/" % (settings.BASE_DIR, portal, tipo)):
        os.mkdir("%s/static/%s/%s/" % (settings.BASE_DIR, portal, tipo))

#   setting elastic params -------------------------------------------
    es = Elasticsearch(port=9200)
#   data values , we create a magazine item here ---------------------
    d = {}
    d['created_at'] = datetime.datetime.now()
    d['CVE_ART'] = 1
    d['tipo'] = tipo
    d['cve_empresa'] = 1  # data['titulo']
    d['status'] = 0
    d['fechapub'] = datetime.datetime.now()

#   crop procecing---------------------------------------------------

    # calling libs ---------------------------------------------------
    from PIL import Image
    from io import BytesIO

    # image params --------------------------------------------
    filefi = '%s' % request.POST.get('imgUrl', None)

    filefi = filefi.decode('base64')

    # creating a image-----------------------------------------------------
    original = Image.open(BytesIO(filefi))

    # cropping data--------------------------------------------------------
    data = request.POST.copy()
    x1 = int(float(data['imgX1']))
    y1 = int(float(data['imgY1']))
    x2 = int(float(data['cropW'])) + x1
    y2 = int(float(data['cropH'])) + y1
    w = int(float(data['imgW']))
    h = int(float(data['imgH']))

    box = (x1, y1, x2, y2)
    resizing = original.resize((w, h), Image.ANTIALIAS)
    cropped = resizing.crop(box)

    if not uid:
        res = es.create(index="pecuarios", doc_type='magazine',
                        ignore=400, body=d)
        uid = res['_id']
    else:
        res = es.update(index="pecuarios", doc_type='magazine',
                        id=uid, body={'doc': d})
        remover = '%s/static/%s/%s/%s.%s' % (
            settings.BASE_DIR, portal, tipo, uid, original.format.lower())
        os.remove(remover)

    nameImage = '%s/static/%s/%s/%s.%s' % (
        settings.BASE_DIR, portal, tipo, uid, original.format.lower())
    nameimagedb = '/static/%s/%s/%s.%s' % (portal,
                                           tipo, uid, original.format.lower())

    cropped.save(nameImage, original.format)

    es.update(index='pecuarios', doc_type='magazine', id=uid,
              body={"doc": {"fotoprincipal": nameimagedb}})

    # es.indices.refresh(index='gdata34')
    response = {"status": "success", 'uids': uid,
                "url": '%s' % (nameimagedb)}
    response = simplejson.dumps(response)

    return HttpResponse(response)
