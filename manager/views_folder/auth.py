# -*- coding: utf-8 -*-
#
"""User login/authentication views."""

from django.shortcuts import redirect, render
from django.views.generic import View
from django.core.mail import send_mail, BadHeaderError
from django.core.exceptions import ValidationError
from django.core.validators import validate_email
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from pecuarios.settings import EMAIL_HOST

from ..models import *


@login_required(login_url='/manager/login/')
def index(request):
    """URL redirect if is login or not."""
    template = 'manager/index.html'
    context = {}
    return render(request, template, context)


class LoginView(View):
    """Login autentication View."""

    template = "manager/auth/login.html"

    def get(self, request):
        """Deprecated."""
        context = {}
        context["next"] = request.GET.get("next", 'manager/')

        return render(request, self.template, context=context)

    def post(self, request):
        """Check the data autentication to login."""
        username = request.POST['username']
        password = request.POST['password']
        next_url = request.POST.get("next", '/manager/')

        user = authenticate(username=username, password=password)

        # import pdb; pdb.set_trace()
        if user is not None:
            login(request, user)

            if user.is_superuser:
                return redirect("/admin")
            else:
                return redirect(next_url)
        else:
            context = {}
            message = "El usuario o la contraseña es incorrecta.Favor de intentar de nuevo."
            context["message"] = message

        return render(request, self.template, context)


class LogoutView(View):
    """User logout view."""

    def get(self, request):
        """Logout and redirect."""
        logout(request)
        return redirect('/manager/login/')


class ProfileView(View):
    """User information view."""

    context = {}
    template = "manager/auth/profile.html"

    def get(self, request):
        """Retrieve the user profile."""
        self.context['status'] = ''
        self.context['user'] = request.user
        return render(request, self.template, self.context)

    def post(self, request):
        """Save profile changes."""
        user = User.objects.get(pk=request.user.pk)
        user_update = request.POST.copy()
        user_update['date_joined'] = user.date_joined

        user_form = UserForm(data=user_update, instance=user)

        if user_form.is_valid():
            user_form.save()
            self.context['status'] = 'El perfil ha sido actualizado corretamente.'
            return redirect('/manager/')
        else:
            self.context['status'] = user_form.errors
            self.context['user'] = user_update
            return render(request, self.template, self.context)


class PassChangeView(View):
    """Change the password."""

    context = {}
    template = "manager/auth/pass_change.html"

    def get(self, request):
        """Load the empty tempalte."""
        self.context['status'] = ''
        return render(request, self.template, self.context)

    def post(self, request):
        """Change the user password."""
        user = User.objects.get(pk=request.user.pk)
        pass1 = request.POST.get('pass1', 'None')
        pass2 = request.POST.get('pass2', None)

        if pass1 == pass2:
            user.set_password(pass1)
            user.save()
            return redirect('/manager/')

        else:
            self.context['status'] = 'Las contraseñas no son iguales. Favor de intentarolo nuevamente.'

        return render(request, self.template, self.context)


class PassRetrieveView(View):
    """Retrieve the password."""

    template = 'manager/auth/pass_retrieve.html'
    context = {}

    def get(self, request):
        """Retieve the user password."""
        return render(request, self.template, self.context)

    def post(self, request):
        """Retieve the user password."""
        email = request.POST.get('email', None)

        from_email = EMAIL_HOST
        to_email = 'ente011@gmail.com'

        if email is not None:
            try:
                validate_email(email)

                user = User.objects.get(email=email)
                subject = u'Recuperación de contraseña'
                message = u'''
                    Su contraseña actual es %s
                    Para la restauración de contraseña:
                    1.- Ingrese con la contraseña proporcionada.
                    2.- Cambie su contraseña por seguridad.

                    Sistema de Pecuarios''' % user.password

                send_mail(subject, message, from_email, [to_email])
                self.context['status'] = 'La contraseña ha sido enviada al correo con el que fue registrado.'

            except ValidationError:
                self.context['status'] = 'El correo no se encuentra registrado en el sistema.'

            except BadHeaderError:
                self.context['status'] = 'Invalid header found.'

        return render(request, self.template, self.context)


LoginView = LoginView.as_view()
LogoutView = LogoutView.as_view()

ProfileView = ProfileView.as_view()
PassChangeView = PassChangeView.as_view()
PassRetrieveView = PassRetrieveView.as_view()
