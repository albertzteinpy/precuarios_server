# -*- coding: utf-8 -*-
"""
Views to managment companies.

This module is used to the managment the companies in the content manage
system.
"""

from django.http import HttpResponse
from django.shortcuts import redirect, render, render_to_response
from django.template.context import RequestContext
from django.views.generic import View

from elasticsearch import Elasticsearch

PER_PAGE = 30


class EmpresasView(View):
    """View to retrieve companies list."""

    template = 'manager/common/empresas.html'

    def get(self, request):
        """Retrieve the list of the all companies."""
        page = request.GET.get('page', 1)
        portal = request.GET.get('portal', None)

        if portal is None:
            response = render(request, self.template, None)
            return response

        es = Elasticsearch(port=9200)
        empresas_list = es.search(
            index="pecuarios",
            doc_type='empresas',
            body={
                'from': '%s' % page,
                'size': '%s' % PER_PAGE,
                'query': {
                    'match_all': {}},
                'sort': {
                    'fecha_alta': {'order': 'desc'},
                }
            })

        empresas = []
        for h in empresas_list['hits']['hits']:
            empresa = {}
            empresa['id'] = h['_id']
            empresa.update(h['_source'])

            empresas.append(empresa)

        context = {}
        total_empresas = empresas_list['hits']['total']

        context['paginator'] = {
            'page': page,
            'page_range': range(1, total_empresas / PER_PAGE + 2),
            'portal': portal,
        }
        context['empresas'] = empresas
        context['total_empresas'] = total_empresas

        response = render(request, self.template, context)
        return response

EmpresasView = EmpresasView.as_view()


class EmpresasDetailView(View):
    """View to retrieve detail company."""

    def get(self, request, empresa_id):
        """Retrieve a single company."""
        request_context = RequestContext(request)
        empresa_form = EmpresaForm

        empresa_id = request.GET.get('empresa_id', None)
        es = Elasticsearch(port=9200)

        if empresa_id is not None:
            data = es.get(index="pecuarios",
                          doc_type='empresas', id=empresa_id)
        else:
            data = None

        args = {}
        context['data'] = data['_source']
        empresa_form = empresa_form(initial=data['_source'])
        context['empresa_form'] = empresa_form
        context['id'] = empresa

        template = 'manager/formempresas.html'
        response = render_to_response(template, context, request_context)
        return response

    def post(self, request):
        """Update a single company."""
        request_context = RequestContext(request)
        empresa_form = EmpresaForm

        empresa_id = request.GET.get('empresa_id', None)
        es = Elasticsearch(port=9200)

        if empresa_id is not None:
            data = es.get(index="pecuarios",
                          doc_type='empresas', id=empresa_id)
        else:
            data = None

        args = {}
        context['data'] = data['_source']
        empresa_form = empresa_form(initial=data['_source'])
        context['empresa_form'] = empresa_form
        context['id'] = empresa

        template = 'manager/formempresas.html'
        response = render_to_response(template, context, request_context)
        return response

EmpresasDetailView.as_view()


class EmpresasCreateView(View):
    """Create a new company."""

    template = 'manager/common/empresas_create.html'

    def get(self, request):
        """Load the template and the empty form."""
        response = render(request, self.template, context={})
        return response


    def post(self, request):
        """Save the company."""
        args = {}
        context = RequestContext(request)
        d = {}
        data = request.POST.copy()
        data['cve_empresa'] = 1
        forma = EmpresaForm
        forma = forma(data)

        del data['csrfmiddlewaretoken']
        del data['id']

        if forma.is_valid():
            response = {'si': 'sii'}
            es = Elasticsearch(port=9200)
            res = es.create(
                index="pecuarios",
                doc_type='empresas',
                ignore=400,
                body=data)

        else:
            response = {'errors': forma.errors}

        return HttpResponse(simplejson.dumps(response))

EmpresasCreateView = EmpresasCreateView.as_view()


class EmpresasDeleteView(View):
    """Delete company."""

    def post(self, request):
        """Delete company by document id."""
        page = request.GET.get('page', None)
        portal = request.GET.get('portal', None)

        document_id = request.POST.get('documentID', None)

        try:
            es = Elasticsearch(port=9200)
            es.delete(
                index="pecuarios",
                doc_type='empresas',
                id=document_id,
            )
        except NotFoundError:
            print 'La empresa ha eliminar no fue encontrada en la db'

        url = '/manager/empresas/?page=%s&portal=%s' % (page, portal)
        return redirect(url)

EmpresasDeleteView = EmpresasDeleteView.as_view()
