# -*- coding: utf-8 -*-


from django.http import HttpResponse
from django.shortcuts import redirect, render, render_to_response
from django.template.context import RequestContext
from django.views.generic import View
from pecuarios.forms import * 
from manager.models import * 
from django.contrib.sites.models import Site
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
import os
from django.template.defaultfilters import slugify
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.forms.models import model_to_dict
from django.contrib.auth.models import User
import re
from django.db.models import Q





class EventosView(View):
    """View to retrieve companies list."""

    template = 'manager/eventos/eventos/listado.html'

    def get(self, request):
        """Retrieve the list of the all companies."""
        page = request.GET.get('page', 1)
        portal = request.GET.get('portal', None)

        context = {}
        caps = Eventos.objects.all()
        context['caps'] = caps

        response = render(request, self.template, context)
        return response

EventosView = EventosView.as_view()




class getEventosView(View):

    def post(self,request):
        victim = request.POST.get('pk',None)
        a = Eventos.objects.get(pk=victim)        
        a.delete()
        return HttpResponse(a)

    def get(self, request):
        #page = request.GET.get('page', 1)
        #portal = request.GET.get('portal', None)
        queries = Q()
        options = {}
        for k in ('nombre_autor__icontains', 'email'):
            value = request.GET.get(k,None)

            if value:
                options[k]=value
                
                queries |= Q(**options)

        context = {}
        autores = Eventos.objects.filter(*[queries],**{})
        arts = [{
                'pk':x.pk,
                'title':x.nombre_autor,
                'descp':x.desc_usuario
                }
                for x in autores ]

        response = simplejson.dumps(arts)
        response = HttpResponse(response)
        
        return response

getEventosView = getEventosView.as_view()



class FormEventosView(View):
    template = 'manager/eventos/eventos/formulario.html'
    def get(self, request):
        #sites = Site.objects.all()

        ida = request.GET.get('id',None)
        if ida:
            instanced = Eventos.objects.get(pk=ida)
        else:
            instanced = None


        #page = request.GET.get('page', 1)
        #portal = request.GET.get('portal', None)
        widgets = {
            'slug':forms.HiddenInput(),
            #'tipo':forms.HiddenInput(),
            'fotoprincipal':forms.HiddenInput(),
            #'fecini':forms.HiddenInput(),
            #'fecfin':forms.HiddenInput(),
        }

        f =  FormCreator()

        forma = f.advanced_form_to_model(modelo=Eventos,
                                         excludes={'privado'},
                                         widgets=widgets)
        

        for x,y in forma.base_fields.iteritems():
            #y.widget_attrs={'class':'monolito'}
            (y.widget.attrs.update({'class':'form-control'}))
        forma = forma(instance=instanced)        
        context = {}
        context['forma']=forma
        context['id']=ida

        response = render(request, self.template, context)
        return response

FormEventosView = FormEventosView.as_view()



class addEventoView(View):
    #template = 'manager/magazine/form_autor.html'
    def post(self, request):
        #sites = Site.objects.all()
        data = request.POST.copy()
        data['slug']=slugify(data['nombre_evento'])
        ida = request.POST.get('id',None)
        if ida:
            instanced = Eventos.objects.get(pk=ida)
            mesagge = u'El registro se actualizó con éxito.'

        else:
            instanced = None
            mesagge = u'El registro se agregó con éxito.' 


        f =  FormCreator()

        forma = f.advanced_form_to_model(modelo=Eventos,excludes={})
        forma = forma(data,request.FILES,instance=instanced)
        try:
            forma.imagen = request.FILES['imagen']
        except:
            pass
        context = {}

        if forma.is_valid():
            save_reg = forma.save()
            responde = {
                'saved':'ok',
                'datos':{'pk':save_reg.pk,
                         'mesagge':mesagge,
                        },
                'callback':'autorSaved'
            }
            
            return HttpResponse(simplejson.dumps(responde))
        else:
            returner = {'errors':forma.errors}
            return HttpResponse(simplejson.dumps(returner))
        
        response = HttpResponse({'algo sa':'lio mal'})


addEventoView = addEventoView.as_view()