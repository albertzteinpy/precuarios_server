from rest_framework import serializers
from manager.models import *

class ArticulosSerializer(serializers.ModelSerializer):
    class Meta:
        model = Articulos
        fields = ('id', 'nombre_articulo',)

class EmpresaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Empresa
        #fields = ('id', 'nombre', 'apellido',)