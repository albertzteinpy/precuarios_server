# -*- coding: utf-8 -*-
"""Proyect url's."""

from django.conf.urls import patterns, url


urlpatterns = patterns(
    'restapp.views',
    #url(r'^$', 'IndexView', name='inicio'),
    #url(r'^home/$', 'HomeView', name='home'),
    url(r'^articulos/$', 'ArtView', name='articulos'),
    url(r'^destacados_carousel/$', 'DestacadosView', name='destacados'),

    #url(r'^formarticulo/$', 'FormArtView', name='formarticulo'),
    #url(r'^addarticulo/$', 'AddArtView', name='addarticulo'),
    #url(r'^crop/$', 'crop', name='crop'),
    #url(r'^getarticulos/$', 'getArtView', name='getarticulos'),
    #url(r'^addphoto/$', 'addphoto', name='addphoto'),
    #url(r'^muser/$', 'muser', name='muser'),
)
