# -*- coding: utf-8 -*-


from django.http import HttpResponse
from django.shortcuts import redirect, render, render_to_response
from django.template.context import RequestContext
from django.views.generic import View
from django.http import JsonResponse
from manager.models import * 
from django.forms.models import model_to_dict
from django.contrib.sites.shortcuts import get_current_site

class ArtView(View):
    """View to retrieve companies list."""


    def get(self, request):
        """Retrieve the list of the all companies."""
        page = request.GET.get('page', 1)
        portal = request.GET.get('portal', None)
        context = {}
        current_site = get_current_site(request)

        arts = Articulos.objects.all().order_by('?')[:3]

        artsjson = [
        		{'id':x.pk,
        		'nombre_articulo':x.nombre_articulo,
        		'texto':x.texto,
        		'imagenprincipal':'http://%s%s'%(current_site.domain,x.fotoprincipal)}

        		 for x in arts]
        context['codigo']='200'
        context['mensaje']='Datos obtenidos'
        context['data']=artsjson
        data = simplejson.dumps(context)

        response = HttpResponse(data,content_type="application/json")
        return response




ArtView = ArtView.as_view()





class DestacadosView(View):
    """View to retrieve companies list."""


    def get(self, request):
        """Retrieve the list of the all companies."""
        page = request.GET.get('page', 1)
        portal = request.GET.get('portal', None)
        context = {}
        current_site = get_current_site(request)

        arts = Articulos.objects.all()[:3]


        artsjson = [
        		{'id':x.pk,
        		'nombre_articulo':x.nombre_articulo,
        		'texto':x.texto,
        		'imagenprincipal':'http://%s%s'%(current_site.domain,x.fotoprincipal)}

        		 for x in arts]

        context['codigo']='200'
        context['mensaje']='Datos obtenidos'
        context['data']=artsjson
        data = simplejson.dumps(context)

        response = HttpResponse(data,content_type="application/json")
        return response




DestacadosView = DestacadosView.as_view()




