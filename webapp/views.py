# -*- coding: utf-8 -*-


from django.http import HttpResponse
from django.shortcuts import redirect, render, render_to_response
from django.template.context import RequestContext
from django.views.generic import View
from pecuarios.forms import * 
from manager.models import * 
from django.contrib.sites.models import Site
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
import os
from django.template.defaultfilters import slugify
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.forms.models import model_to_dict
from django.contrib.sites.shortcuts import get_current_site
import datetime
from django.db.models import Q




def banners(request,section='diamante'):

    current_site = get_current_site(request)
    if section in request.COOKIES:
        diamante = simplejson.loads(request.COOKIES[section])
    else:
        diamante = []

    bann_diamante_shows = 5

    if len(diamante) > 0:
        diams = [x['pk'] for x in diamante]
    else:
        diams = []

    bandiamante_list = Banner.objects.filter(~Q(id__in=diams),plan=section,portal=current_site)
    

    for abb in bandiamante_list:
        abb.contador += 1
        abb.save()

    bandiamante_list = bandiamante_list.values('pk','imagencuadrado','imagenlargo','liga')


    newdiamantes = [b for b in bandiamante_list]
    i = 0
    if len(newdiamantes)<bann_diamante_shows:
        resto = bann_diamante_shows - len(newdiamantes)
        actual = len(newdiamantes)

        for r in range(resto):
            actual+=1
            try:
                newdiamantes.append(diamante[r])
            except:
                pass
    return newdiamantes

class IndexView(View):
    """View to retrieve companies list."""

    template = 'webapp/home.html'

    def get(self, request):
    	context = {}
    	current_site = get_current_site(request)
        newdiamantes = banners(request,section='diamante')
        bannersoro = banners(request,section='oro')
        context['current_site'] =current_site
        context['secciones'] = Seccion.objects.filter(seccion='home',portal_seccion=current_site)
        context['videos'] = Videos.objects.filter(portal=current_site).order_by('-pk')
        context['noticias_des'] = Articulos.objects.filter(portal_articulo=current_site,fecha_des_end__gte=datetime.datetime.now())
        context['noticias'] = Articulos.objects.filter(Q(fecha_des_end__lte=datetime.datetime.now())| Q(fecha_des_beg__isnull=True))
        context['diamante'] = newdiamantes
        context['bannersoro'] = bannersoro
        context['eventos'] = Eventos.objects.filter(portal=current_site)

        response = render(request, self.template, context)
        

        response.set_cookie('diamante', simplejson.dumps(newdiamantes))
        response.set_cookie('oro', simplejson.dumps(bannersoro))
        
        return response

IndexView = IndexView.as_view()


class anview(View):
    """View to retrieve companies list."""

    template = 'webapp/google9961f820608b1944.html'

    def get(self, request):
        context = {}


        response = render(request, self.template, context)
        return response

anview = anview.as_view()




class SingleView(View):
    """View to retrieve companies list."""

    template = 'webapp/single.html'

    def get(self, request,slugg):
        context = {}
        current_site = get_current_site(request)
        #myapp = apps.get_app_config(current_site.name)

        #context['current_site'] =current_site
        #context['secciones'] = Seccion.objects.filter(seccion='home',portal_seccion=current_site)
        #context['noticias_des'] = Articulos.objects.filter(portal_articulo=current_site,fecha_des_end__gte=datetime.datetime.now())
        try: 
            art = Articulos.objects.get(Q(slug=slugg))
            art.vistas += 1
            art.save()
            context['noticias'] = art
        except:
            context['noticias'] = None

        response = render(request, self.template, context)
        return response

SingleView = SingleView.as_view()



class ArticulosView(View):
    """View to retrieve companies list."""

    template = 'webapp/articulos.html'

    def get(self, request):
        context = {}
        current_site = get_current_site(request)
        #myapp = apps.get_app_config(current_site.name)

        context['current_site'] =current_site
        

        response = render(request, self.template, context)
        return response

ArticulosView = ArticulosView.as_view()

class getArtView(View):

    template = 'manager/magazine/articulos.html'
    def put(self,request):
        return HttpResponse("{'as':'asas'}")

    def get(self, request):
        #page = request.GET.get('page', 1)
        #portal = request.GET.get('portal', None)
        context = {}
        articulos = Articulos.objects.all()
        arts = [{
                'pk':x.pk,
                'title':x.nombre_articulo,
                'image':x.fotoprincipal,
                'descp':x.texto,
                'votos':x.numvotos
                }
                for x in articulos ]

        response = simplejson.dumps(arts)
        response = HttpResponse(response)
        #context['articulos'] = ['a','b']
        #context['total_empresas'] = 100
        #response = render(request, self.template, context)
        
        return response

getArtView = getArtView.as_view()
