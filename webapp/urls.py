# -*- coding: utf-8 -*-

from django.conf.urls import patterns, url

urlpatterns = patterns(
    'webapp.views',
    url(r'^$', 'IndexView', name='inicio'),
    url(r'^google9961f820608b1944.html$', 'anview', name='google9961f820608b1944.html'),
    url(r'^single/(?P<slugg>[-\w]+)$', 'SingleView', name='SingleView'),
    #url(r'^getarticulos/$', 'getArtView', name='getarticulos'),
)
