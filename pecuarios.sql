-- MySQL dump 10.13  Distrib 5.5.49, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: pecuarios
-- ------------------------------------------------------
-- Server version	5.5.49-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_group_permissions_group_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  KEY `auth_group_permissi_permission_id_84c5c92e_fk_auth_permission_id` (`permission_id`),
  CONSTRAINT `auth_group_permissi_permission_id_84c5c92e_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_permission_content_type_id_01ab375a_uniq` (`content_type_id`,`codename`),
  CONSTRAINT `auth_permissi_content_type_id_2f476e4b_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=187 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(30) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `auth_user_groups`
--

DROP TABLE IF EXISTS `auth_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_groups_user_id_94350c0c_uniq` (`user_id`,`group_id`),
  KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`),
  CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `auth_user_user_permissions`
--

DROP TABLE IF EXISTS `auth_user_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_user_permissions_user_id_14a6b632_uniq` (`user_id`,`permission_id`),
  KEY `auth_user_user_perm_permission_id_1fbb5f2c_fk_auth_permission_id` (`permission_id`),
  CONSTRAINT `auth_user_user_perm_permission_id_1fbb5f2c_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin__content_type_id_c4bce8eb_fk_django_content_type_id` (`content_type_id`),
  KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`),
  CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `django_admin__content_type_id_c4bce8eb_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_76bd3d3b_uniq` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `django_migrations`
--

DROP TABLE IF EXISTS `django_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_de54fa62` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `django_site`
--

DROP TABLE IF EXISTS `django_site`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_site` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `domain` varchar(100) NOT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_site_domain_a2e37b91_uniq` (`domain`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `manager_artautores`
--

DROP TABLE IF EXISTS `manager_artautores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager_artautores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subindice` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `participacion` varchar(200) NOT NULL,
  `artitulopk_id` int(11) NOT NULL,
  `autorpk_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `manager_artautores_f94243a5` (`artitulopk_id`),
  KEY `manager_artautores_fc290afe` (`autorpk_id`),
  CONSTRAINT `manager_artautores_autorpk_id_756d8d13_fk_manager_autores_id` FOREIGN KEY (`autorpk_id`) REFERENCES `manager_autores` (`id`),
  CONSTRAINT `manager_artautore_artitulopk_id_7408d5a6_fk_manager_articulos_id` FOREIGN KEY (`artitulopk_id`) REFERENCES `manager_articulos` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `manager_articulos`
--

DROP TABLE IF EXISTS `manager_articulos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager_articulos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(500) NOT NULL,
  `nombre_articulo` varchar(500) NOT NULL,
  `fechapub` datetime NOT NULL,
  `texto` longtext,
  `descrip` longtext NOT NULL,
  `fecha_alta` datetime NOT NULL,
  `fecha_des_beg` datetime DEFAULT NULL,
  `fecha_des_end` datetime DEFAULT NULL,
  `status` varchar(10) NOT NULL,
  `tipo` varchar(100) NOT NULL,
  `vistas` int(11) NOT NULL,
  `vistasapp` int(11) NOT NULL,
  `votacion` decimal(18,0) NOT NULL,
  `numvotos` int(11) NOT NULL,
  `fotoprincipal` varchar(600) NOT NULL,
  `privado` tinyint(1) NOT NULL,
  `likes` int(11) NOT NULL,
  `cve_cat_id` int(11) NOT NULL,
  `cve_empresa_id` int(11),
  `portal_articulo_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `manager_articulos_2e7e084d` (`cve_cat_id`),
  KEY `manager_articulos_5c6dec1c` (`cve_empresa_id`),
  KEY `manager_articulos_473ac7bf` (`portal_articulo_id`),
  CONSTRAINT `manager_articulos_portal_articulo_id_d2087a5f_fk_django_site_id` FOREIGN KEY (`portal_articulo_id`) REFERENCES `django_site` (`id`),
  CONSTRAINT `manager_articulos_cve_empresa_id_30a5e4b3_fk_manager_empresa_id` FOREIGN KEY (`cve_empresa_id`) REFERENCES `manager_empresa` (`id`),
  CONSTRAINT `manager_arti_cve_cat_id_e86d470f_fk_manager_categoriamagazine_id` FOREIGN KEY (`cve_cat_id`) REFERENCES `manager_categoriamagazine` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `manager_artscomentarios`
--

DROP TABLE IF EXISTS `manager_artscomentarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager_artscomentarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `comentario` longtext NOT NULL,
  `autorizado` tinyint(1) NOT NULL,
  `fecha_alta` datetime NOT NULL,
  `articulopk_id` int(11) NOT NULL,
  `usuariocomemnta_id` int(11),
  PRIMARY KEY (`id`),
  KEY `manager_artscomen_articulopk_id_9122860d_fk_manager_articulos_id` (`articulopk_id`),
  KEY `manager_artscomentarios_2d38d406` (`usuariocomemnta_id`),
  CONSTRAINT `manager_artsco_usuariocomemnta_id_b033aa11_fk_manager_tblregs_id` FOREIGN KEY (`usuariocomemnta_id`) REFERENCES `manager_tblregs` (`id`),
  CONSTRAINT `manager_artscomen_articulopk_id_9122860d_fk_manager_articulos_id` FOREIGN KEY (`articulopk_id`) REFERENCES `manager_articulos` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `manager_autores`
--

DROP TABLE IF EXISTS `manager_autores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager_autores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug_nombre_autor` varchar(500) NOT NULL,
  `nombre_autor` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `redes` longtext,
  `info` longtext,
  `vistas` decimal(18,0) DEFAULT NULL,
  `votacion` decimal(18,0) DEFAULT NULL,
  `numvotos` decimal(18,0) DEFAULT NULL,
  `tipo` varchar(100) NOT NULL,
  `desc_usuario` longtext,
  `privado` tinyint(1) NOT NULL,
  `fotoprincipal` varchar(600) DEFAULT NULL,
  `empresarel_id` int(11),
  PRIMARY KEY (`id`),
  KEY `manager_autores_dad412a0` (`empresarel_id`),
  CONSTRAINT `manager_autores_empresarel_id_1bf43d64_fk_manager_empresa_id` FOREIGN KEY (`empresarel_id`) REFERENCES `manager_empresa` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `manager_banner`
--

DROP TABLE IF EXISTS `manager_banner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager_banner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) DEFAULT NULL,
  `imagen` varchar(100) NOT NULL,
  `liga` varchar(900) DEFAULT NULL,
  `fecha_inicio` datetime NOT NULL,
  `fecha_fin` datetime DEFAULT NULL,
  `seccion` varchar(100) NOT NULL,
  `posicion` smallint(6) DEFAULT NULL,
  `fechapub` datetime NOT NULL,
  `fecha_alta` datetime NOT NULL,
  `status` varchar(10) NOT NULL,
  `contador` int(11) NOT NULL,
  `borde` int(11) DEFAULT NULL,
  `bannerapp` tinyint(1) NOT NULL,
  `tipobanner` varchar(100) NOT NULL,
  `clicks` int(11) NOT NULL,
  `impresiones` int(11) NOT NULL,
  `visible` varchar(50) NOT NULL,
  `patrocinador_id` int(11),
  `portal_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `manager_banner_8d4a6ece` (`patrocinador_id`),
  KEY `manager_banner_ba012feb` (`portal_id`),
  CONSTRAINT `manager_banner_portal_id_88d692e3_fk_django_site_id` FOREIGN KEY (`portal_id`) REFERENCES `django_site` (`id`),
  CONSTRAINT `manager_banner_patrocinador_id_d55d1d73_fk_manager_empresa_id` FOREIGN KEY (`patrocinador_id`) REFERENCES `manager_empresa` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `manager_bolsa`
--

DROP TABLE IF EXISTS `manager_bolsa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager_bolsa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) NOT NULL,
  `profesion` varchar(500) DEFAULT NULL,
  `tipo` varchar(100) DEFAULT NULL,
  `texto` varchar(8000) DEFAULT NULL,
  `imagen` varchar(100) DEFAULT NULL,
  `fechavence` datetime DEFAULT NULL,
  `fecha_alta` datetime NOT NULL,
  `fechamod` date NOT NULL,
  `status` varchar(10) NOT NULL,
  `vistas` decimal(18,0) DEFAULT NULL,
  `likes` int(11) NOT NULL,
  `interesados` int(11) NOT NULL,
  `cve_estado_id` int(11) NOT NULL,
  `cve_pais_id` int(11) NOT NULL,
  `portal_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `manager_bolsa_7b784449` (`cve_estado_id`),
  KEY `manager_bolsa_accaa61e` (`cve_pais_id`),
  KEY `manager_bolsa_ba012feb` (`portal_id`),
  CONSTRAINT `manager_bolsa_portal_id_8a6484e4_fk_django_site_id` FOREIGN KEY (`portal_id`) REFERENCES `django_site` (`id`),
  CONSTRAINT `manager_bolsa_cve_estado_id_80588fcb_fk_manager_tblestados_id` FOREIGN KEY (`cve_estado_id`) REFERENCES `manager_tblestados` (`id`),
  CONSTRAINT `manager_bolsa_cve_pais_id_ea4815ef_fk_manager_tblpaises_id` FOREIGN KEY (`cve_pais_id`) REFERENCES `manager_tblpaises` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `manager_bolsaempresarel`
--

DROP TABLE IF EXISTS `manager_bolsaempresarel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager_bolsaempresarel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bolsapk_id` int(11) NOT NULL,
  `contacto_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `manager_bolsaempresarel_bolsapk_id_15926942_fk_manager_bolsa_id` (`bolsapk_id`),
  KEY `manager_bolsaempresarel_debcd608` (`contacto_id`),
  CONSTRAINT `manager_bolsa_contacto_id_31778e3b_fk_manager_contactoempresa_id` FOREIGN KEY (`contacto_id`) REFERENCES `manager_contactoempresa` (`id`),
  CONSTRAINT `manager_bolsaempresarel_bolsapk_id_15926942_fk_manager_bolsa_id` FOREIGN KEY (`bolsapk_id`) REFERENCES `manager_bolsa` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `manager_bolsafiles`
--

DROP TABLE IF EXISTS `manager_bolsafiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager_bolsafiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `archivo` varchar(100) NOT NULL,
  `fecha_alta` datetime DEFAULT NULL,
  `bolsapk_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `manager_bolsafiles_bolsapk_id_99f00fdd_fk_manager_bolsa_id` (`bolsapk_id`),
  CONSTRAINT `manager_bolsafiles_bolsapk_id_99f00fdd_fk_manager_bolsa_id` FOREIGN KEY (`bolsapk_id`) REFERENCES `manager_bolsa` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `manager_bolsalikes`
--

DROP TABLE IF EXISTS `manager_bolsalikes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager_bolsalikes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` datetime NOT NULL,
  `visitaapp` int(11) DEFAULT NULL,
  `cve_bolsa_id` int(11) NOT NULL,
  `cve_reg_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `manager_bolsalikes_cve_bolsa_id_742ac96f_fk_manager_bolsa_id` (`cve_bolsa_id`),
  KEY `manager_bolsalikes_777fcef3` (`cve_reg_id`),
  CONSTRAINT `manager_bolsalikes_cve_reg_id_bddbb650_fk_manager_tblregs_id` FOREIGN KEY (`cve_reg_id`) REFERENCES `manager_tblregs` (`id`),
  CONSTRAINT `manager_bolsalikes_cve_bolsa_id_742ac96f_fk_manager_bolsa_id` FOREIGN KEY (`cve_bolsa_id`) REFERENCES `manager_bolsa` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `manager_bolsamsg`
--

DROP TABLE IF EXISTS `manager_bolsamsg`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager_bolsamsg` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mensaje` longtext,
  `fecha_alta` datetime DEFAULT NULL,
  `archivo` varchar(100) DEFAULT NULL,
  `cve_bolsa_id` int(11) NOT NULL,
  `cve_reg_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `manager_bolsamsg_cve_bolsa_id_0f331ea0_fk_manager_bolsa_id` (`cve_bolsa_id`),
  KEY `manager_bolsamsg_777fcef3` (`cve_reg_id`),
  CONSTRAINT `manager_bolsamsg_cve_reg_id_2259b621_fk_manager_tblregs_id` FOREIGN KEY (`cve_reg_id`) REFERENCES `manager_tblregs` (`id`),
  CONSTRAINT `manager_bolsamsg_cve_bolsa_id_0f331ea0_fk_manager_bolsa_id` FOREIGN KEY (`cve_bolsa_id`) REFERENCES `manager_bolsa` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `manager_bolsasolicitante`
--

DROP TABLE IF EXISTS `manager_bolsasolicitante`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager_bolsasolicitante` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bolsapk_id` int(11) NOT NULL,
  `publicadopor_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `manager_bolsasolicitante_bolsapk_id_dcf32d75_fk_manager_bolsa_id` (`bolsapk_id`),
  KEY `manager_bolsasolicitante_838654ae` (`publicadopor_id`),
  CONSTRAINT `manager_bolsasoli_publicadopor_id_53a007c9_fk_manager_tblregs_id` FOREIGN KEY (`publicadopor_id`) REFERENCES `manager_tblregs` (`id`),
  CONSTRAINT `manager_bolsasolicitante_bolsapk_id_dcf32d75_fk_manager_bolsa_id` FOREIGN KEY (`bolsapk_id`) REFERENCES `manager_bolsa` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `manager_bolsavistas`
--

DROP TABLE IF EXISTS `manager_bolsavistas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager_bolsavistas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` datetime NOT NULL,
  `visitaapp` int(11) DEFAULT NULL,
  `cve_bolsa_id` int(11) NOT NULL,
  `cve_reg_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `manager_bolsavistas_cve_bolsa_id_588105b0_fk_manager_bolsa_id` (`cve_bolsa_id`),
  KEY `manager_bolsavistas_777fcef3` (`cve_reg_id`),
  CONSTRAINT `manager_bolsavistas_cve_reg_id_bd710b46_fk_manager_tblregs_id` FOREIGN KEY (`cve_reg_id`) REFERENCES `manager_tblregs` (`id`),
  CONSTRAINT `manager_bolsavistas_cve_bolsa_id_588105b0_fk_manager_bolsa_id` FOREIGN KEY (`cve_bolsa_id`) REFERENCES `manager_bolsa` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `manager_capsulas`
--

DROP TABLE IF EXISTS `manager_capsulas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager_capsulas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_capsula` varchar(255) NOT NULL,
  `descrip` longtext NOT NULL,
  `texto` longtext NOT NULL,
  `fotoprincipal` varchar(600) NOT NULL,
  `fechapub` datetime NOT NULL,
  `video` varchar(255) DEFAULT NULL,
  `fecha_alta` datetime NOT NULL,
  `status` varchar(10) NOT NULL,
  `descrip2` longtext,
  `posicion_video` int(11) DEFAULT NULL,
  `publicar_app_y_movil` tinyint(1) NOT NULL,
  `vistas` int(11) NOT NULL,
  `vistasapp` int(11) NOT NULL,
  `votacion` decimal(18,0) NOT NULL,
  `numvotos` int(11) NOT NULL,
  `privado` tinyint(1) NOT NULL,
  `likes` int(11) NOT NULL,
  `patricnador_id` int(11),
  `portal_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `manager_capsulas_369198df` (`patricnador_id`),
  KEY `manager_capsulas_ba012feb` (`portal_id`),
  CONSTRAINT `manager_capsulas_portal_id_5a3cabf5_fk_django_site_id` FOREIGN KEY (`portal_id`) REFERENCES `django_site` (`id`),
  CONSTRAINT `manager_capsulas_patricnador_id_52a9c56d_fk_manager_empresa_id` FOREIGN KEY (`patricnador_id`) REFERENCES `manager_empresa` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `manager_categoriamagazine`
--

DROP TABLE IF EXISTS `manager_categoriamagazine`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager_categoriamagazine` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_categoria` varchar(255) NOT NULL,
  `fechapub` datetime NOT NULL,
  `actv` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `manager_categoriamagazine_portal`
--

DROP TABLE IF EXISTS `manager_categoriamagazine_portal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager_categoriamagazine_portal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `categoriamagazine_id` int(11) NOT NULL,
  `site_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `manager_categoriamagazine_por_categoriamagazine_id_7379794c_uniq` (`categoriamagazine_id`,`site_id`),
  KEY `manager_categoriamagazine_por_site_id_4d7b9954_fk_django_site_id` (`site_id`),
  CONSTRAINT `manager_categoriamagazine_por_site_id_4d7b9954_fk_django_site_id` FOREIGN KEY (`site_id`) REFERENCES `django_site` (`id`),
  CONSTRAINT `ma_categoriamagazine_id_3ed077dd_fk_manager_categoriamagazine_id` FOREIGN KEY (`categoriamagazine_id`) REFERENCES `manager_categoriamagazine` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `manager_catprods`
--

DROP TABLE IF EXISTS `manager_catprods`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager_catprods` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(500) NOT NULL,
  `nombre_categoria` varchar(255) NOT NULL,
  `portal` int(11) DEFAULT NULL,
  `fecha_alta` datetime NOT NULL,
  `imagen` varchar(100) DEFAULT NULL,
  `parentcat_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `manager_catprods_parentcat_id_c798839c_fk_manager_catprods_id` (`parentcat_id`),
  CONSTRAINT `manager_catprods_parentcat_id_c798839c_fk_manager_catprods_id` FOREIGN KEY (`parentcat_id`) REFERENCES `manager_catprods` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `manager_clasificadolike`
--

DROP TABLE IF EXISTS `manager_clasificadolike`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager_clasificadolike` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha_alta` datetime NOT NULL,
  `cve_clasif_id` int(11) NOT NULL,
  `cve_reg_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `manager_clasificadolike_9463eae8` (`cve_clasif_id`),
  KEY `manager_clasificadolike_777fcef3` (`cve_reg_id`),
  CONSTRAINT `manager_clasificadolik_cve_reg_id_3766afc8_fk_manager_tblregs_id` FOREIGN KEY (`cve_reg_id`) REFERENCES `manager_tblregs` (`id`),
  CONSTRAINT `manager_clasif_cve_clasif_id_d1f2d930_fk_manager_clasificados_id` FOREIGN KEY (`cve_clasif_id`) REFERENCES `manager_clasificados` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `manager_clasificados`
--

DROP TABLE IF EXISTS `manager_clasificados`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager_clasificados` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) NOT NULL,
  `slug` varchar(500) NOT NULL,
  `portal` int(11) NOT NULL,
  `texto` longtext NOT NULL,
  `fechavence` datetime NOT NULL,
  `fecha_alta` datetime NOT NULL,
  `precio` double DEFAULT NULL,
  `status` varchar(10) NOT NULL,
  `vistas` int(11) NOT NULL,
  `vistasapp` int(11) NOT NULL,
  `votacion` decimal(18,0) NOT NULL,
  `numvotos` int(11) NOT NULL,
  `fotoprincipal` varchar(600) NOT NULL,
  `privado` tinyint(1) NOT NULL,
  `likes` int(11) NOT NULL,
  `cve_estado_id` int(11) NOT NULL,
  `cve_pais_id` int(11) NOT NULL,
  `cve_reg_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `manager_clasificados_7b784449` (`cve_estado_id`),
  KEY `manager_clasificados_accaa61e` (`cve_pais_id`),
  KEY `manager_clasificados_777fcef3` (`cve_reg_id`),
  CONSTRAINT `manager_clasificados_cve_reg_id_7a9c6434_fk_manager_tblregs_id` FOREIGN KEY (`cve_reg_id`) REFERENCES `manager_tblregs` (`id`),
  CONSTRAINT `manager_clasificado_cve_pais_id_d79cdcb9_fk_manager_tblpaises_id` FOREIGN KEY (`cve_pais_id`) REFERENCES `manager_tblpaises` (`id`),
  CONSTRAINT `manager_clasific_cve_estado_id_d1cbbe2b_fk_manager_tblestados_id` FOREIGN KEY (`cve_estado_id`) REFERENCES `manager_tblestados` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `manager_clasificadosmsg`
--

DROP TABLE IF EXISTS `manager_clasificadosmsg`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager_clasificadosmsg` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mensaje` longtext,
  `fecha_alta` datetime NOT NULL,
  `status` varchar(10) NOT NULL,
  `cve_clasif_id` int(11) NOT NULL,
  `cve_reg_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `manager_clasif_cve_clasif_id_1b98de04_fk_manager_clasificados_id` (`cve_clasif_id`),
  KEY `manager_clasificadosmsg_777fcef3` (`cve_reg_id`),
  CONSTRAINT `manager_clasificadosms_cve_reg_id_521700b9_fk_manager_tblregs_id` FOREIGN KEY (`cve_reg_id`) REFERENCES `manager_tblregs` (`id`),
  CONSTRAINT `manager_clasif_cve_clasif_id_1b98de04_fk_manager_clasificados_id` FOREIGN KEY (`cve_clasif_id`) REFERENCES `manager_clasificados` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `manager_contactoempresa`
--

DROP TABLE IF EXISTS `manager_contactoempresa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager_contactoempresa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(500) NOT NULL,
  `nombre_contacto` varchar(200) NOT NULL,
  `email_contacto` varchar(500) NOT NULL,
  `redes` longtext,
  `fotoprincipal` varchar(255) DEFAULT NULL,
  `cve_estado_id` int(11),
  `cve_pais_id` int(11),
  `empresapk_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `manager_contactoempresa_7b784449` (`cve_estado_id`),
  KEY `manager_contactoempresa_accaa61e` (`cve_pais_id`),
  KEY `manager_contactoempresa_48557666` (`empresapk_id`),
  CONSTRAINT `manager_contactoempr_empresapk_id_fa65708a_fk_manager_empresa_id` FOREIGN KEY (`empresapk_id`) REFERENCES `manager_empresa` (`id`),
  CONSTRAINT `manager_contactoemp_cve_pais_id_3970f66a_fk_manager_tblpaises_id` FOREIGN KEY (`cve_pais_id`) REFERENCES `manager_tblpaises` (`id`),
  CONSTRAINT `manager_contacto_cve_estado_id_3871d9b5_fk_manager_tblestados_id` FOREIGN KEY (`cve_estado_id`) REFERENCES `manager_tblestados` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `manager_empresa`
--

DROP TABLE IF EXISTS `manager_empresa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager_empresa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug_empresa` varchar(500) NOT NULL,
  `nombre_empresa` varchar(255) NOT NULL,
  `categorias` longtext NOT NULL,
  `desc_empresa` longtext NOT NULL,
  `direcc` varchar(400) NOT NULL,
  `cp` varchar(10) NOT NULL,
  `tels` varchar(255) DEFAULT NULL,
  `fax` varchar(255) DEFAULT NULL,
  `web` varchar(900) DEFAULT NULL,
  `email` varchar(6000) DEFAULT NULL,
  `fotoprincipal` varchar(255) DEFAULT NULL,
  `fecha_alta` datetime NOT NULL,
  `status` varchar(100) NOT NULL,
  `vistas` decimal(18,0) DEFAULT NULL,
  `votacion` decimal(18,0) DEFAULT NULL,
  `numvotos` decimal(18,0) DEFAULT NULL,
  `privado` tinyint(1) NOT NULL,
  `redes` longtext,
  `nivel_empresa` varchar(100) NOT NULL,
  `latitud` varchar(255) DEFAULT NULL,
  `longitud` varchar(255) DEFAULT NULL,
  `cve_estado_id` int(11),
  `cve_pais_id` int(11),
  `portal_empresa_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `manager_empresa_7b784449` (`cve_estado_id`),
  KEY `manager_empresa_accaa61e` (`cve_pais_id`),
  KEY `manager_empresa_6e5dfb43` (`portal_empresa_id`),
  CONSTRAINT `manager_empresa_portal_empresa_id_82ae5a29_fk_django_site_id` FOREIGN KEY (`portal_empresa_id`) REFERENCES `django_site` (`id`),
  CONSTRAINT `manager_empresa_cve_estado_id_d4dbb319_fk_manager_tblestados_id` FOREIGN KEY (`cve_estado_id`) REFERENCES `manager_tblestados` (`id`),
  CONSTRAINT `manager_empresa_cve_pais_id_cd0551cf_fk_manager_tblpaises_id` FOREIGN KEY (`cve_pais_id`) REFERENCES `manager_tblpaises` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `manager_encuestas`
--

DROP TABLE IF EXISTS `manager_encuestas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager_encuestas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) DEFAULT NULL,
  `fecha_alta` datetime NOT NULL,
  `desde` datetime DEFAULT NULL,
  `hasta` datetime DEFAULT NULL,
  `fechas` longtext,
  `status` varchar(10) NOT NULL,
  `vistas` int(11) NOT NULL,
  `vistasapp` int(11) NOT NULL,
  `votacion` decimal(18,0) NOT NULL,
  `numvotos` int(11) NOT NULL,
  `privado` tinyint(1) NOT NULL,
  `likes` int(11) NOT NULL,
  `cve_empresa_id` int(11) DEFAULT NULL,
  `portal_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `manager_encuestas_cve_empresa_id_60c1a8f6_fk_manager_empresa_id` (`cve_empresa_id`),
  KEY `manager_encuestas_portal_id_81e3b573_fk_django_site_id` (`portal_id`),
  CONSTRAINT `manager_encuestas_portal_id_81e3b573_fk_django_site_id` FOREIGN KEY (`portal_id`) REFERENCES `django_site` (`id`),
  CONSTRAINT `manager_encuestas_cve_empresa_id_60c1a8f6_fk_manager_empresa_id` FOREIGN KEY (`cve_empresa_id`) REFERENCES `manager_empresa` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `manager_eventos`
--

DROP TABLE IF EXISTS `manager_eventos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager_eventos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_evento` varchar(255) NOT NULL,
  `ciudad` varchar(255) NOT NULL,
  `info` longtext NOT NULL,
  `programa` longtext NOT NULL,
  `resenia` longtext,
  `anteceds` longtext,
  `participa` int(11) DEFAULT NULL,
  `fecini` datetime NOT NULL,
  `fecfin` datetime NOT NULL,
  `logo` varchar(100) DEFAULT NULL,
  `logopec` varchar(100) DEFAULT NULL,
  `fecha_alta` datetime NOT NULL,
  `status` smallint(6) DEFAULT NULL,
  `fechas` longtext,
  `emails` varchar(6000) DEFAULT NULL,
  `descrip` longtext,
  `imgprog` varchar(100) DEFAULT NULL,
  `datoscontacto` longtext,
  `publicidad` varchar(255) DEFAULT NULL,
  `video` varchar(255) DEFAULT NULL,
  `latitud` varchar(50) DEFAULT NULL,
  `longitud` varchar(50) DEFAULT NULL,
  `vistas` int(11) NOT NULL,
  `vistasapp` int(11) NOT NULL,
  `votacion` decimal(18,0) NOT NULL,
  `numvotos` int(11) NOT NULL,
  `fotoprincipal` varchar(600) NOT NULL,
  `privado` tinyint(1) NOT NULL,
  `likes` int(11) NOT NULL,
  `cve_empresa_id` int(11) DEFAULT NULL,
  `cve_estado_id` int(11) NOT NULL,
  `cve_pais_id` int(11) NOT NULL,
  `portal_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `manager_eventos_cve_empresa_id_cf57e99b_fk_manager_empresa_id` (`cve_empresa_id`),
  KEY `manager_eventos_7b784449` (`cve_estado_id`),
  KEY `manager_eventos_accaa61e` (`cve_pais_id`),
  KEY `manager_eventos_ba012feb` (`portal_id`),
  CONSTRAINT `manager_eventos_portal_id_0f6d2e94_fk_django_site_id` FOREIGN KEY (`portal_id`) REFERENCES `django_site` (`id`),
  CONSTRAINT `manager_eventos_cve_empresa_id_cf57e99b_fk_manager_empresa_id` FOREIGN KEY (`cve_empresa_id`) REFERENCES `manager_empresa` (`id`),
  CONSTRAINT `manager_eventos_cve_estado_id_a80be731_fk_manager_tblestados_id` FOREIGN KEY (`cve_estado_id`) REFERENCES `manager_tblestados` (`id`),
  CONSTRAINT `manager_eventos_cve_pais_id_f08a1061_fk_manager_tblpaises_id` FOREIGN KEY (`cve_pais_id`) REFERENCES `manager_tblpaises` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `manager_forocat`
--

DROP TABLE IF EXISTS `manager_forocat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager_forocat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_categoria` varchar(255) NOT NULL,
  `fechapub` datetime NOT NULL,
  `actv` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `manager_forocat_portal`
--

DROP TABLE IF EXISTS `manager_forocat_portal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager_forocat_portal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `forocat_id` int(11) NOT NULL,
  `site_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `manager_forocat_portal_forocat_id_6616673e_uniq` (`forocat_id`,`site_id`),
  KEY `manager_forocat_portal_site_id_df338d9f_fk_django_site_id` (`site_id`),
  CONSTRAINT `manager_forocat_portal_site_id_df338d9f_fk_django_site_id` FOREIGN KEY (`site_id`) REFERENCES `django_site` (`id`),
  CONSTRAINT `manager_forocat_portal_forocat_id_65deee3a_fk_manager_forocat_id` FOREIGN KEY (`forocat_id`) REFERENCES `manager_forocat` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `manager_foromensajes`
--

DROP TABLE IF EXISTS `manager_foromensajes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager_foromensajes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `texto` longtext,
  `autorizado` int(11) DEFAULT NULL,
  `fecha_alta` datetime NOT NULL,
  `status` varchar(10) NOT NULL,
  `cve_foro_id` int(11) NOT NULL,
  `cve_reg_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `manager_foromensajes_5f4fc96e` (`cve_foro_id`),
  KEY `manager_foromensajes_777fcef3` (`cve_reg_id`),
  CONSTRAINT `manager_foromensajes_cve_reg_id_821215ef_fk_manager_tblregs_id` FOREIGN KEY (`cve_reg_id`) REFERENCES `manager_tblregs` (`id`),
  CONSTRAINT `manager_foromensajes_cve_foro_id_79e810a7_fk_manager_foros_id` FOREIGN KEY (`cve_foro_id`) REFERENCES `manager_foros` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `manager_foros`
--

DROP TABLE IF EXISTS `manager_foros`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager_foros` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(500) NOT NULL,
  `portal` int(11) NOT NULL,
  `ciudad` varchar(255) DEFAULT NULL,
  `texto` longtext NOT NULL,
  `fecha_alta` datetime NOT NULL,
  `status` varchar(10) NOT NULL,
  `vistas` int(11) NOT NULL,
  `vistasapp` int(11) NOT NULL,
  `votacion` decimal(18,0) NOT NULL,
  `numvotos` int(11) NOT NULL,
  `privado` tinyint(1) NOT NULL,
  `likes` int(11) NOT NULL,
  `autor_id` int(11) NOT NULL,
  `categoria_id` int(11) NOT NULL,
  `cve_estado_id` int(11) NOT NULL,
  `cve_pais_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `manager_foros_52be3978` (`autor_id`),
  KEY `manager_foros_daf3833b` (`categoria_id`),
  KEY `manager_foros_7b784449` (`cve_estado_id`),
  KEY `manager_foros_accaa61e` (`cve_pais_id`),
  CONSTRAINT `manager_foros_cve_pais_id_f88d679e_fk_manager_tblpaises_id` FOREIGN KEY (`cve_pais_id`) REFERENCES `manager_tblpaises` (`id`),
  CONSTRAINT `manager_foros_autor_id_1d09c3f7_fk_manager_tblregs_id` FOREIGN KEY (`autor_id`) REFERENCES `manager_tblregs` (`id`),
  CONSTRAINT `manager_foros_categoria_id_88f4f00f_fk_manager_forocat_id` FOREIGN KEY (`categoria_id`) REFERENCES `manager_forocat` (`id`),
  CONSTRAINT `manager_foros_cve_estado_id_efdc39bc_fk_manager_tblestados_id` FOREIGN KEY (`cve_estado_id`) REFERENCES `manager_tblestados` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `manager_galeriaarticulos`
--

DROP TABLE IF EXISTS `manager_galeriaarticulos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager_galeriaarticulos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `foto` varchar(100) NOT NULL,
  `idnoticia` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `manager_galeriaclasificados`
--

DROP TABLE IF EXISTS `manager_galeriaclasificados`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager_galeriaclasificados` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `foto` varchar(100) NOT NULL,
  `clasificadopk_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `manager_gal_clasificadopk_id_4154a636_fk_manager_clasificados_id` (`clasificadopk_id`),
  CONSTRAINT `manager_gal_clasificadopk_id_4154a636_fk_manager_clasificados_id` FOREIGN KEY (`clasificadopk_id`) REFERENCES `manager_clasificados` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `manager_galeriaempresas`
--

DROP TABLE IF EXISTS `manager_galeriaempresas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager_galeriaempresas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipoimagen` varchar(100) NOT NULL,
  `imagen` varchar(500) DEFAULT NULL,
  `link` varchar(500) DEFAULT NULL,
  `empresa_own_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `manager_galeriaemp_empresa_own_id_42f0e044_fk_manager_empresa_id` (`empresa_own_id`),
  CONSTRAINT `manager_galeriaemp_empresa_own_id_42f0e044_fk_manager_empresa_id` FOREIGN KEY (`empresa_own_id`) REFERENCES `manager_empresa` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `manager_likearticulos`
--

DROP TABLE IF EXISTS `manager_likearticulos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager_likearticulos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` datetime NOT NULL,
  `articulopk_id` int(11) NOT NULL,
  `usuarioreg_id` int(11),
  PRIMARY KEY (`id`),
  KEY `manager_likeartic_articulopk_id_79b7d82e_fk_manager_articulos_id` (`articulopk_id`),
  KEY `manager_likearticulos_c3c0266d` (`usuarioreg_id`),
  CONSTRAINT `manager_likearticul_usuarioreg_id_dfc111a1_fk_manager_tblregs_id` FOREIGN KEY (`usuarioreg_id`) REFERENCES `manager_tblregs` (`id`),
  CONSTRAINT `manager_likeartic_articulopk_id_79b7d82e_fk_manager_articulos_id` FOREIGN KEY (`articulopk_id`) REFERENCES `manager_articulos` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `manager_likecapsulas`
--

DROP TABLE IF EXISTS `manager_likecapsulas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager_likecapsulas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` datetime NOT NULL,
  `capsulapk_id` int(11) NOT NULL,
  `usuarioreg_id` int(11),
  PRIMARY KEY (`id`),
  KEY `manager_likecapsula_capsulapk_id_2b73caf3_fk_manager_capsulas_id` (`capsulapk_id`),
  KEY `manager_likecapsulas_c3c0266d` (`usuarioreg_id`),
  CONSTRAINT `manager_likecapsula_usuarioreg_id_129c14c4_fk_manager_tblregs_id` FOREIGN KEY (`usuarioreg_id`) REFERENCES `manager_tblregs` (`id`),
  CONSTRAINT `manager_likecapsula_capsulapk_id_2b73caf3_fk_manager_capsulas_id` FOREIGN KEY (`capsulapk_id`) REFERENCES `manager_capsulas` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `manager_likeeventos`
--

DROP TABLE IF EXISTS `manager_likeeventos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager_likeeventos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` datetime NOT NULL,
  `usuarioreg_id` int(11) NOT NULL,
  `videopk_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `manager_likeeventos_c3c0266d` (`usuarioreg_id`),
  KEY `manager_likeeventos_aada0a4d` (`videopk_id`),
  CONSTRAINT `manager_likeeventos_videopk_id_a0084071_fk_manager_eventos_id` FOREIGN KEY (`videopk_id`) REFERENCES `manager_eventos` (`id`),
  CONSTRAINT `manager_likeeventos_usuarioreg_id_641bf276_fk_manager_tblregs_id` FOREIGN KEY (`usuarioreg_id`) REFERENCES `manager_tblregs` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `manager_likeproducto`
--

DROP TABLE IF EXISTS `manager_likeproducto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager_likeproducto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` datetime NOT NULL,
  `productopk_id` int(11) NOT NULL,
  `usuarioreg_id` int(11),
  PRIMARY KEY (`id`),
  KEY `manager_likeproducto_e936764e` (`productopk_id`),
  KEY `manager_likeproducto_c3c0266d` (`usuarioreg_id`),
  CONSTRAINT `manager_likeproduct_usuarioreg_id_cac8aa77_fk_manager_tblregs_id` FOREIGN KEY (`usuarioreg_id`) REFERENCES `manager_tblregs` (`id`),
  CONSTRAINT `manager_likeproduc_productopk_id_d379adca_fk_manager_producto_id` FOREIGN KEY (`productopk_id`) REFERENCES `manager_producto` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `manager_likevideos`
--

DROP TABLE IF EXISTS `manager_likevideos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager_likevideos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` datetime NOT NULL,
  `usuarioreg_id` int(11),
  `videopk_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `manager_likevideos_c3c0266d` (`usuarioreg_id`),
  KEY `manager_likevideos_aada0a4d` (`videopk_id`),
  CONSTRAINT `manager_likevideos_videopk_id_6a2ce17b_fk_manager_capsulas_id` FOREIGN KEY (`videopk_id`) REFERENCES `manager_capsulas` (`id`),
  CONSTRAINT `manager_likevideos_usuarioreg_id_0d0d753d_fk_manager_tblregs_id` FOREIGN KEY (`usuarioreg_id`) REFERENCES `manager_tblregs` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `manager_opcionpregunta`
--

DROP TABLE IF EXISTS `manager_opcionpregunta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager_opcionpregunta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `opcion` varchar(500) NOT NULL,
  `numvotos` int(11) NOT NULL,
  `preguntapk_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `manager_opcionpregunta_0e4d995e` (`preguntapk_id`),
  CONSTRAINT `manager_op_preguntapk_id_86c162b4_fk_manager_preguntaencuesta_id` FOREIGN KEY (`preguntapk_id`) REFERENCES `manager_preguntaencuesta` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `manager_preguntaencuesta`
--

DROP TABLE IF EXISTS `manager_preguntaencuesta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager_preguntaencuesta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pregunta` varchar(500) NOT NULL,
  `encuestapk_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `manager_preguntae_encuestapk_id_8a45e72f_fk_manager_encuestas_id` (`encuestapk_id`),
  CONSTRAINT `manager_preguntae_encuestapk_id_8a45e72f_fk_manager_encuestas_id` FOREIGN KEY (`encuestapk_id`) REFERENCES `manager_encuestas` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `manager_prodcontacto`
--

DROP TABLE IF EXISTS `manager_prodcontacto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager_prodcontacto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contactopk_id` int(11) NOT NULL,
  `productopk_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `manager_pro_contactopk_id_265f7415_fk_manager_contactoempresa_id` (`contactopk_id`),
  KEY `manager_prodcontacto_e936764e` (`productopk_id`),
  CONSTRAINT `manager_prodcontac_productopk_id_8c207043_fk_manager_producto_id` FOREIGN KEY (`productopk_id`) REFERENCES `manager_producto` (`id`),
  CONSTRAINT `manager_pro_contactopk_id_265f7415_fk_manager_contactoempresa_id` FOREIGN KEY (`contactopk_id`) REFERENCES `manager_contactoempresa` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `manager_producto`
--

DROP TABLE IF EXISTS `manager_producto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager_producto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(500) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `portal` int(11) NOT NULL,
  `tipo` int(11) DEFAULT NULL,
  `resenia` longtext,
  `liga` varchar(900) DEFAULT NULL,
  `fecha_alta` datetime NOT NULL,
  `status` varchar(10) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `descrip` longtext,
  `vistas` int(11) NOT NULL,
  `vistasapp` int(11) NOT NULL,
  `votacion` decimal(18,0) NOT NULL,
  `numvotos` int(11) NOT NULL,
  `fotoprincipal` varchar(600) NOT NULL,
  `privado` tinyint(1) NOT NULL,
  `likes` int(11) NOT NULL,
  `cvecat_id` int(11) NOT NULL,
  `empresapk_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `manager_producto_cvecat_id_89561f6e_fk_manager_catprods_id` (`cvecat_id`),
  KEY `manager_producto_empresapk_id_b50fff3a_fk_manager_empresa_id` (`empresapk_id`),
  CONSTRAINT `manager_producto_empresapk_id_b50fff3a_fk_manager_empresa_id` FOREIGN KEY (`empresapk_id`) REFERENCES `manager_empresa` (`id`),
  CONSTRAINT `manager_producto_cvecat_id_89561f6e_fk_manager_catprods_id` FOREIGN KEY (`cvecat_id`) REFERENCES `manager_catprods` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `manager_razas`
--

DROP TABLE IF EXISTS `manager_razas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager_razas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  `texto` longtext,
  `fecha_alta` datetime NOT NULL,
  `status` varchar(10) NOT NULL,
  `descrip` longtext NOT NULL,
  `cve_empresa_id` int(11) DEFAULT NULL,
  `portal_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `manager_razas_cve_empresa_id_87a4e278_fk_manager_empresa_id` (`cve_empresa_id`),
  KEY `manager_razas_portal_id_c0b12ad1_fk_django_site_id` (`portal_id`),
  CONSTRAINT `manager_razas_portal_id_c0b12ad1_fk_django_site_id` FOREIGN KEY (`portal_id`) REFERENCES `django_site` (`id`),
  CONSTRAINT `manager_razas_cve_empresa_id_87a4e278_fk_manager_empresa_id` FOREIGN KEY (`cve_empresa_id`) REFERENCES `manager_empresa` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `manager_tblbannerspatrocinadores`
--

DROP TABLE IF EXISTS `manager_tblbannerspatrocinadores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager_tblbannerspatrocinadores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) DEFAULT NULL,
  `imagen` varchar(100) DEFAULT NULL,
  `status` smallint(6) DEFAULT NULL,
  `impresiones` int(11) NOT NULL,
  `cve_empresa_id` int(11) DEFAULT NULL,
  `portal_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `manager_tblbanners_cve_empresa_id_6bb0a69f_fk_manager_empresa_id` (`cve_empresa_id`),
  KEY `manager_tblbannerspatrocina_portal_id_74a6cc0c_fk_django_site_id` (`portal_id`),
  CONSTRAINT `manager_tblbannerspatrocina_portal_id_74a6cc0c_fk_django_site_id` FOREIGN KEY (`portal_id`) REFERENCES `django_site` (`id`),
  CONSTRAINT `manager_tblbanners_cve_empresa_id_6bb0a69f_fk_manager_empresa_id` FOREIGN KEY (`cve_empresa_id`) REFERENCES `manager_empresa` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `manager_tblestados`
--

DROP TABLE IF EXISTS `manager_tblestados`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager_tblestados` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) DEFAULT NULL,
  `status` smallint(6) DEFAULT NULL,
  `cve_pais_id` int(11),
  PRIMARY KEY (`id`),
  KEY `manager_tblestados_accaa61e` (`cve_pais_id`),
  CONSTRAINT `manager_tblestados_cve_pais_id_c00fb487_fk_manager_tblpaises_id` FOREIGN KEY (`cve_pais_id`) REFERENCES `manager_tblpaises` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=897 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `manager_tbleveempresas`
--

DROP TABLE IF EXISTS `manager_tbleveempresas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager_tbleveempresas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cve_empresa_id` int(11) NOT NULL,
  `cve_evento_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `manager_tbleveempr_cve_empresa_id_eb0d9fcb_fk_manager_empresa_id` (`cve_empresa_id`),
  KEY `manager_tbleveempre_cve_evento_id_61533ee2_fk_manager_eventos_id` (`cve_evento_id`),
  CONSTRAINT `manager_tbleveempre_cve_evento_id_61533ee2_fk_manager_eventos_id` FOREIGN KEY (`cve_evento_id`) REFERENCES `manager_eventos` (`id`),
  CONSTRAINT `manager_tbleveempr_cve_empresa_id_eb0d9fcb_fk_manager_empresa_id` FOREIGN KEY (`cve_empresa_id`) REFERENCES `manager_empresa` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `manager_tblevesimgs`
--

DROP TABLE IF EXISTS `manager_tblevesimgs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager_tblevesimgs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cve_img` varchar(100) NOT NULL,
  `cve_evento_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `manager_tblevesimgs_cve_evento_id_b2d65da3_fk_manager_eventos_id` (`cve_evento_id`),
  CONSTRAINT `manager_tblevesimgs_cve_evento_id_b2d65da3_fk_manager_eventos_id` FOREIGN KEY (`cve_evento_id`) REFERENCES `manager_eventos` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `manager_tblexpositores`
--

DROP TABLE IF EXISTS `manager_tblexpositores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager_tblexpositores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  `cv` longtext,
  `email` varchar(500) NOT NULL,
  `tels` varchar(255) DEFAULT NULL,
  `fotoprincipal` varchar(600) NOT NULL,
  `fecha_alta` datetime NOT NULL,
  `status` varchar(10) NOT NULL,
  `url` varchar(511) DEFAULT NULL,
  `vistas` int(11) NOT NULL,
  `vistasapp` int(11) NOT NULL,
  `votacion` decimal(18,0) NOT NULL,
  `numvotos` int(11) NOT NULL,
  `likes` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `manager_tbljimimgs`
--

DROP TABLE IF EXISTS `manager_tbljimimgs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager_tbljimimgs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cve_img` varchar(100) DEFAULT NULL,
  `cve_jim_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `manager_tbljimimgs_f1b9b0a8` (`cve_jim_id`),
  CONSTRAINT `manager_tbljimimgs_cve_jim_id_2f50bc91_fk_manager_tbljimlong_id` FOREIGN KEY (`cve_jim_id`) REFERENCES `manager_tbljimlong` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `manager_tbljimlong`
--

DROP TABLE IF EXISTS `manager_tbljimlong`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager_tbljimlong` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  `texto` longtext,
  `fecha` datetime DEFAULT NULL,
  `fecha_alta` datetime NOT NULL,
  `status` varchar(10) NOT NULL,
  `visible` varchar(50) NOT NULL,
  `empresapk_id` int(11) DEFAULT NULL,
  `portal_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `manager_tbljimlong_empresapk_id_3e942744_fk_manager_empresa_id` (`empresapk_id`),
  KEY `manager_tbljimlong_portal_id_4bedb977_fk_django_site_id` (`portal_id`),
  CONSTRAINT `manager_tbljimlong_portal_id_4bedb977_fk_django_site_id` FOREIGN KEY (`portal_id`) REFERENCES `django_site` (`id`),
  CONSTRAINT `manager_tbljimlong_empresapk_id_3e942744_fk_manager_empresa_id` FOREIGN KEY (`empresapk_id`) REFERENCES `manager_empresa` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `manager_tblpaises`
--

DROP TABLE IF EXISTS `manager_tblpaises`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager_tblpaises` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) DEFAULT NULL,
  `status` smallint(6) DEFAULT NULL,
  `bandera` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `manager_tblpush`
--

DROP TABLE IF EXISTS `manager_tblpush`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager_tblpush` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  `lada` varchar(10) DEFAULT NULL,
  `tel` varchar(50) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `medio` smallint(6) DEFAULT NULL,
  `dia` smallint(6) DEFAULT NULL,
  `hora` smallint(6) DEFAULT NULL,
  `bandera` smallint(6) DEFAULT NULL,
  `fecha_alta` datetime NOT NULL,
  `status` varchar(10) NOT NULL,
  `fecha_contacto` datetime DEFAULT NULL,
  `tipotel` smallint(6) DEFAULT NULL,
  `cve_estado_id` int(11) NOT NULL,
  `cve_pais_id` int(11) NOT NULL,
  `cve_reg_id` int(11) NOT NULL,
  `empresa_id` int(11),
  `portal_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `manager_tblpush_cve_estado_id_e97c4d2e_fk_manager_tblestados_id` (`cve_estado_id`),
  KEY `manager_tblpush_cve_pais_id_2d04c42c_fk_manager_tblpaises_id` (`cve_pais_id`),
  KEY `manager_tblpush_777fcef3` (`cve_reg_id`),
  KEY `manager_tblpush_e8f8b1ef` (`empresa_id`),
  KEY `manager_tblpush_ba012feb` (`portal_id`),
  CONSTRAINT `manager_tblpush_portal_id_c22bc66b_fk_django_site_id` FOREIGN KEY (`portal_id`) REFERENCES `django_site` (`id`),
  CONSTRAINT `manager_tblpush_cve_estado_id_e97c4d2e_fk_manager_tblestados_id` FOREIGN KEY (`cve_estado_id`) REFERENCES `manager_tblestados` (`id`),
  CONSTRAINT `manager_tblpush_cve_pais_id_2d04c42c_fk_manager_tblpaises_id` FOREIGN KEY (`cve_pais_id`) REFERENCES `manager_tblpaises` (`id`),
  CONSTRAINT `manager_tblpush_cve_reg_id_77227018_fk_manager_tblregs_id` FOREIGN KEY (`cve_reg_id`) REFERENCES `manager_tblregs` (`id`),
  CONSTRAINT `manager_tblpush_empresa_id_be05df65_fk_manager_empresa_id` FOREIGN KEY (`empresa_id`) REFERENCES `manager_empresa` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `manager_tblrazasimgs`
--

DROP TABLE IF EXISTS `manager_tblrazasimgs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager_tblrazasimgs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `imagen` varchar(100) NOT NULL,
  `cve_raza_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `manager_tblrazasimgs_cve_raza_id_a442c9e3_fk_manager_razas_id` (`cve_raza_id`),
  CONSTRAINT `manager_tblrazasimgs_cve_raza_id_a442c9e3_fk_manager_razas_id` FOREIGN KEY (`cve_raza_id`) REFERENCES `manager_razas` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `manager_tblregs`
--

DROP TABLE IF EXISTS `manager_tblregs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager_tblregs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(100) NOT NULL,
  `apa` varchar(100) NOT NULL,
  `ama` varchar(100) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `fecnac` datetime DEFAULT NULL,
  `pswd` varchar(20) DEFAULT NULL,
  `ocupacion` int(11) DEFAULT NULL,
  `tipoprod` int(11) DEFAULT NULL,
  `subtipoprod` int(11) DEFAULT NULL,
  `numreprod` int(11) DEFAULT NULL,
  `recibenl` smallint(6) DEFAULT NULL,
  `fecha_alta` datetime DEFAULT NULL,
  `status` smallint(6) DEFAULT NULL,
  `portal` varchar(20) DEFAULT NULL,
  `validado` smallint(6) DEFAULT NULL,
  `cve_empresa` varchar(100) DEFAULT NULL,
  `numaccesos` decimal(18,0) DEFAULT NULL,
  `token` varchar(2000) DEFAULT NULL,
  `cve_id` int(11) DEFAULT NULL,
  `cve_estado_id` int(11) DEFAULT NULL,
  `cve_pais_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `manager_tblregs_cve_id_624e1cfd_fk_auth_user_id` (`cve_id`),
  KEY `manager_tblregs_cve_estado_id_afaaeb21_fk_manager_tblestados_id` (`cve_estado_id`),
  KEY `manager_tblregs_cve_pais_id_d96bdeb8_fk_manager_tblpaises_id` (`cve_pais_id`),
  CONSTRAINT `manager_tblregs_cve_pais_id_d96bdeb8_fk_manager_tblpaises_id` FOREIGN KEY (`cve_pais_id`) REFERENCES `manager_tblpaises` (`id`),
  CONSTRAINT `manager_tblregs_cve_estado_id_afaaeb21_fk_manager_tblestados_id` FOREIGN KEY (`cve_estado_id`) REFERENCES `manager_tblestados` (`id`),
  CONSTRAINT `manager_tblregs_cve_id_624e1cfd_fk_auth_user_id` FOREIGN KEY (`cve_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `manager_tbltipscoments`
--

DROP TABLE IF EXISTS `manager_tbltipscoments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager_tbltipscoments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `texto` longtext,
  `fecha_alta` datetime NOT NULL,
  `status` varchar(10) NOT NULL,
  `vistas` int(11) NOT NULL,
  `vistasapp` int(11) NOT NULL,
  `votacion` decimal(18,0) NOT NULL,
  `likes` int(11) NOT NULL,
  `cve_reg_id` int(11) NOT NULL,
  `cve_tip_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `manager_tbltipscoments_cve_reg_id_c864ab91_fk_manager_tblregs_id` (`cve_reg_id`),
  KEY `manager_tbltipscoments_d4f7050e` (`cve_tip_id`),
  CONSTRAINT `manager_tbltipscoments_cve_tip_id_63bfbdee_fk_manager_tips_id` FOREIGN KEY (`cve_tip_id`) REFERENCES `manager_tips` (`id`),
  CONSTRAINT `manager_tbltipscoments_cve_reg_id_c864ab91_fk_manager_tblregs_id` FOREIGN KEY (`cve_reg_id`) REFERENCES `manager_tblregs` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `manager_tbltipsvotacion`
--

DROP TABLE IF EXISTS `manager_tbltipsvotacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager_tbltipsvotacion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `valor` int(11) NOT NULL,
  `fecha` datetime NOT NULL,
  `cve_reg_id` int(11) NOT NULL,
  `cve_tip_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `manager_tbltipsvotacio_cve_reg_id_86e046fb_fk_manager_tblregs_id` (`cve_reg_id`),
  KEY `manager_tbltipsvotacion_d4f7050e` (`cve_tip_id`),
  CONSTRAINT `manager_tbltipsvotacion_cve_tip_id_db485309_fk_manager_tips_id` FOREIGN KEY (`cve_tip_id`) REFERENCES `manager_tips` (`id`),
  CONSTRAINT `manager_tbltipsvotacio_cve_reg_id_86e046fb_fk_manager_tblregs_id` FOREIGN KEY (`cve_reg_id`) REFERENCES `manager_tblregs` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `manager_tbltotstips`
--

DROP TABLE IF EXISTS `manager_tbltotstips`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager_tbltotstips` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` datetime DEFAULT NULL,
  `estafirmado` int(11) DEFAULT NULL,
  `numero` decimal(18,0) DEFAULT NULL,
  `cve_tip_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `manager_tbltotstips_d4f7050e` (`cve_tip_id`),
  CONSTRAINT `manager_tbltotstips_cve_tip_id_84ba9588_fk_manager_tips_id` FOREIGN KEY (`cve_tip_id`) REFERENCES `manager_tips` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `manager_tips`
--

DROP TABLE IF EXISTS `manager_tips`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager_tips` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  `descrip` longtext,
  `texto` longtext,
  `fechapub` datetime NOT NULL,
  `video` varchar(255) DEFAULT NULL,
  `fecha_alta` datetime NOT NULL,
  `status` varchar(10) NOT NULL,
  `vistas` int(11) NOT NULL,
  `vistasapp` int(11) NOT NULL,
  `votacion` decimal(18,0) NOT NULL,
  `numvotos` int(11) NOT NULL,
  `fotoprincipal` varchar(600) NOT NULL,
  `privado` tinyint(1) NOT NULL,
  `likes` int(11) NOT NULL,
  `portal_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `manager_tips_portal_id_a0a3c9cd_fk_django_site_id` (`portal_id`),
  CONSTRAINT `manager_tips_portal_id_a0a3c9cd_fk_django_site_id` FOREIGN KEY (`portal_id`) REFERENCES `django_site` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `manager_usresponde`
--

DROP TABLE IF EXISTS `manager_usresponde`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager_usresponde` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `encunestapk_id` int(11) NOT NULL,
  `usuarioreg_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `manager_usrespon_encunestapk_id_dd03a5e4_fk_manager_encuestas_id` (`encunestapk_id`),
  KEY `manager_usresponde_usuarioreg_id_24d3e597_fk_manager_tblregs_id` (`usuarioreg_id`),
  CONSTRAINT `manager_usresponde_usuarioreg_id_24d3e597_fk_manager_tblregs_id` FOREIGN KEY (`usuarioreg_id`) REFERENCES `manager_tblregs` (`id`),
  CONSTRAINT `manager_usrespon_encunestapk_id_dd03a5e4_fk_manager_encuestas_id` FOREIGN KEY (`encunestapk_id`) REFERENCES `manager_encuestas` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `manager_videos`
--

DROP TABLE IF EXISTS `manager_videos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager_videos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo_video` varchar(255) NOT NULL,
  `descrip` longtext NOT NULL,
  `fechapub` datetime NOT NULL,
  `video` varchar(255) DEFAULT NULL,
  `fecha_alta` datetime NOT NULL,
  `status` varchar(10) NOT NULL,
  `publicar_app_y_movil` tinyint(1) NOT NULL,
  `vistas` int(11) NOT NULL,
  `vistasapp` int(11) NOT NULL,
  `votacion` decimal(18,0) NOT NULL,
  `numvotos` int(11) NOT NULL,
  `privado` tinyint(1) NOT NULL,
  `likes` int(11) NOT NULL,
  `empresa_id` int(11) DEFAULT NULL,
  `portal_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `manager_videos_empresa_id_a4da661e_fk_manager_empresa_id` (`empresa_id`),
  KEY `manager_videos_portal_id_c94fe904_fk_django_site_id` (`portal_id`),
  CONSTRAINT `manager_videos_portal_id_c94fe904_fk_django_site_id` FOREIGN KEY (`portal_id`) REFERENCES `django_site` (`id`),
  CONSTRAINT `manager_videos_empresa_id_a4da661e_fk_manager_empresa_id` FOREIGN KEY (`empresa_id`) REFERENCES `manager_empresa` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-05-26 20:39:19
