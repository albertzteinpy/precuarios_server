from django.conf.urls import include, url
from django.contrib import admin
from django.conf.urls.static import static
from django.conf import settings

'''
from restapp.views import ArtViewSet
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'rest_articulos', ArtViewSet)
#router.register(r'autores', AutorViewSet)
'''


urlpatterns = [

    url(r'^manager/', include('manager.urls')),
    url(r'^rest/', include('restapp.urls')),
    url(r'^', include('webapp.urls')), 
    url(r'^admin/', include(admin.site.urls)),
    #url(r'^', include(router.urls)),
    #url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)